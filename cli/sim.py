import argparse
import os
import re
import unittest
import sys
#import traceback
import logging
import cProfile

from simfactory.appdb import appdb
from simfactory.mdb import mdb
from simfactory.paralleltopology import ParallelTopology
from simfactory.paths import basepath
from simfactory.simulation import Simulation
from simfactory.utils import parse_walltime
import simfactory.tests.test_simulation
import fnmatch

from cli.cmd_remote import cmd_execute, cmd_remote, cmd_login, cmd_sync
from cli.utils import (local_machine, current_application,
                       load_simulation, current_build, simulation_exists)

from simfactory.utils import UserException

logger = logging.getLogger('sim') # pylint: disable=C0103

VERSION = '3.0'

def main():
    parser = argparse.ArgumentParser(
        prog='sim',
        description='Simulation Factory %s' % VERSION)

    ###########################################################################
    # General Options
    ###########################################################################
    parser.add_argument(
        '--version', action='version', version='SimFactory %s' % VERSION)

    group_verbose = parser.add_mutually_exclusive_group()
    group_verbose.add_argument(
        '-v', '--verbose', action='store_true', help='make output verbose')
    group_verbose.add_argument(
        '-q', '--quiet', action='store_true',
        help='make output terse and machine-parsable')

    parser.add_argument(
        '--machine', dest='local_machinename', help='name of local machine')
    # BW: Does the --app option really belong here?
    parser.add_argument(
        '--app', dest='application_name', help='name of application')
    parser.add_argument(
        '--directory', help='run SimFactory in this directory')

    ###########################################################################
    # Subcommands
    ###########################################################################
    subparsers = parser.add_subparsers(
        help='must be one of the following subcommands:', metavar='command')

    ##### Selftest ############################################################

    # selftest
    parser_selftest = subparsers.add_parser(
        'selftest', help='run all self-tests')
    parser_selftest.set_defaults(func=cmd_selftest)

    ##### Machines ############################################################

    # list-machines
    parser_list_machines = subparsers.add_parser(
        'list-machines', help='list names of all machines in the MDB')
    parser_list_machines.set_defaults(func=cmd_list_machines)

    # show-machine
    parser_show_machine = subparsers.add_parser(
        'show-machine', help='output an MDB entry')
    parser_show_machine.set_defaults(func=cmd_show_machine)
    parser_show_machine.add_argument(
        'machinename', help='machine name')

    # local-machine
    parser_local_machine = subparsers.add_parser(
        'local-machine', help='show name of local machine')
    parser_local_machine.set_defaults(func=cmd_local_machine)

    ##### Applications ########################################################

    # list-applications
    parser_list_applications = subparsers.add_parser(
        'list-applications', help='list names of all applications in the appdb')
    parser_list_applications.set_defaults(func=cmd_list_applications)

    # show-application
    parser_show_application = subparsers.add_parser(
        'show-application', help='output an appdb entry')
    parser_show_application.set_defaults(func=cmd_show_application)
    parser_show_application.add_argument(
        'appname', help='application name')

    # current-application
    parser_current_application = subparsers.add_parser(
        'current-application', help='show name of current application')
    parser_current_application.set_defaults(func=cmd_current_application)

    # application-path
    parser_application_path = subparsers.add_parser(
        'application-path', help='show base path of application')
    parser_application_path.set_defaults(func=cmd_application_path)

    ##### Remote access ########################################################

    # execute
    parser_execute = subparsers.add_parser(
        'execute', help='execute a shell command')
    parser_execute.set_defaults(func=cmd_execute)
    parser_execute.add_argument(
        'arguments', nargs=argparse.REMAINDER, help='shell command')

    # login
    parser_login = subparsers.add_parser(
        'login', help='call an interactive shell')
    parser_login.set_defaults(func=cmd_login)

    # remote
    parser_remote = subparsers.add_parser(
        'remote', help='run on a remote machine')
    parser_remote.set_defaults(func=cmd_remote)
    parser_remote.add_argument(
        'machinename', help='machine name')
    parser_remote.add_argument(
        'arguments', nargs=argparse.REMAINDER,
        help='arguments for remote SimFactory')

    # sync
    parser_sync = subparsers.add_parser(
        'sync', help='sync source tree to a remote machine')
    parser_sync.set_defaults(func=cmd_sync)
    parser_sync.add_argument(
        'machinename', nargs='+', help='machine names')

    ##### Simulations #########################################################

    # Common options for a list of simulations
    parent_simulations = argparse.ArgumentParser(add_help=False)
    parent_simulations.add_argument(
        'simulations', nargs='*', metavar='simulation', help='simulation name')

    # Options when creating a simulation.  These are stored with the
    # simulation when it is created, whether it is subsequently going
    # to be run or submitted.
    parent_create = argparse.ArgumentParser(add_help=False)
    parent_create.add_argument(
        'simulation', metavar='simulation', help='simulation name')
    parent_create.add_argument(
        '--executable', help='executable file', required=False, default=None)
    parent_create.add_argument(
        '--parfile', help='parameter file', required=False)
    # Will get this from the Application eventually.
    parent_create.add_argument(
        '--parallel', help='whether to run in parallel', required=False,
        default=False, action="store_true")
    parent_create.add_argument(
        '--build', help='build to use', required=False, default=None)
    parent_create.add_argument(
        '--cores', help='number of cores to run on', required=False, type=int)
    parent_create.add_argument(
        '--processes', help='number of processes to run on', required=False,
        type=int)
    parent_create.add_argument(
        '--cores-per-process', help='number of cores to assign to each process', required=False,
        type=int)
    parent_create.add_argument(
        '--processes-per-node', help='number of processes to assign to each node', required=False,
        type=int)
    parent_create.add_argument(
        '--walltime', help='walltime to use', required=False, default=None)
    parent_create.add_argument(
        '--continuous-resubmission', action="store_true",
        help='whether to submit a new segment after each submitted '
        'segment finishes',
        required=False, default=True)
    parent_create.add_argument(
        '--define', help='definition to use', required=False, default=[],
        nargs=2, action="append")
    parent_create.add_argument(
        '--post-simulation-script', help='script to run after a simulation finishes', required=False, default=None)
    parent_create.add_argument(
        '--nodelist', help='nodes on which to run the simulation', required=False, default=None)

    # create
    parser_create = subparsers.add_parser(
        'create', help='create a simulation',
        parents=[parent_create])
    parser_create.set_defaults(func=cmd_create)

    # modify
    parser_modify = subparsers.add_parser(
        'modify', help='modify a simulation',
        parents=[parent_create])
    parser_modify.add_argument(
        '--force', help='allow modification of running or queued simulations', action='store_true')
    parser_modify.set_defaults(func=cmd_modify)

    # submit
    parser_submit = subparsers.add_parser(
        'submit', help='submit a simulation.',
        parents=[parent_create])
    parser_submit.set_defaults(func=cmd_submit)

    # run
    parser_run = subparsers.add_parser(
        'run', help='run a simulation',
        parents=[parent_create])
    parser_run.set_defaults(func=cmd_run)

    # stop
    parser_stop = subparsers.add_parser(
        'stop', help='stop a running simulation', parents=[parent_simulations])
    parser_stop.set_defaults(func=cmd_stop)

    # pause
    parser_pause = subparsers.add_parser(
        'pause', help='pause a simulation so it can be continued later', parents=[parent_simulations])
    parser_pause.set_defaults(func=cmd_pause)

    # status
    parser_status = subparsers.add_parser(
        'status', help='query status of the given simulations, or all '
                       'simulations if no simulations specified',
        parents=[parent_simulations])
    parser_status.add_argument(
        '--all', action="store_true",
        help='whether to show all simulations, including inactive',
        required=False, default=False)
    parser_status.set_defaults(func=cmd_status)

    # deactivate
    parser_deactivate = subparsers.add_parser(
        'deactivate', help='deactivate a stopped simulation',
        parents=[parent_simulations])
    parser_deactivate.set_defaults(func=cmd_deactivate)

    # activate
    parser_activate = subparsers.add_parser(
        'activate', help='activate a deactivated simulation',
        parents=[parent_simulations])
    parser_activate.set_defaults(func=cmd_activate)

    # continue
    parser_continue = subparsers.add_parser(
        'continue', help='continue a stopped simulation',
        parents=[parent_simulations])
    parser_continue.set_defaults(func=cmd_continue)
    parser_continue.add_argument(
        '--force', help='allow continuation of running or queued simulations', action='store_true')

    # update
    parser_update = subparsers.add_parser(
        'update', help='update databases in a simulation',
        parents=[parent_simulations])
    parser_update.set_defaults(func=cmd_update)

    # clone
    parser_clone = subparsers.add_parser(
        'clone', help='clone a simulation')
    parser_clone.set_defaults(func=cmd_clone)

    parser_clone.add_argument(
        'origsim', help='name of simulation to clone')
    parser_clone.add_argument(
        'newsim', help='name of new simulation')

    # ls
    parser_ls = subparsers.add_parser(
        'ls', help='list simulations and their properties',
        parents=[parent_simulations])
    parser_ls.set_defaults(func=cmd_ls)
    parser_ls.add_argument(
        '--format', help='output format', required=False, default='{name}',
        type=str)

    ###########################################################################
    # Parse arguments
    ###########################################################################
    args = parser.parse_args()

    try:
        # Process some global options
        setup_logging(args.verbose)
        setup_local_machine(args)
        setup_directory(args)

        if 'func' in args:
            args.func(args)
        else:
            parser.print_usage()
    except KeyboardInterrupt:
        # Avoid stack backtrace on user interrupts
        logger.info('Interrupted operation.')
        sys.exit(1)

    except UserException as ex:
        logger.error("Error: %s", ex)
#        print(traceback.format_exc())
        sys.exit(1)



# Selftest actions

def cmd_selftest(args):
    verb = 0 if args.quiet else 2
    found_tests = unittest.TestLoader().discover(basepath())
    unittest.TextTestRunner(verbosity=verb).run(found_tests)



# MDB actions

def cmd_list_machines(args):
    if not args.quiet:
        # TODO: think about this
        logger.info('Available machines:')
    for name in sorted(mdb):
        print(name)

def cmd_show_machine(args):
    try:
        machine = mdb[args.machinename]
    except KeyError:
        raise UserException('Unknown machine "'+args.machinename+'"')
    if not args.quiet:
        print('MDB machine entry: %s' % args.machinename)
    for key, value in machine.items():
        # indent continuation lines
        if isinstance(value, list):
            value = '\n'.join(value)
        svalue = str(value)
        svalue = re.sub(r'\n', r'\n    ', svalue)
        print('%s = %s' % (key, svalue))

def cmd_local_machine(args):
    machine = mdb.get_local_machine()
    if not args.quiet:
        print('Local machine: %s' % machine.name)
    else:
        print(machine.name)



# AppDB actions

def cmd_list_applications(args):
    if not args.quiet:
        print('Available applications:')
    for name in sorted(appdb):
        print(name)

def cmd_show_application(args):
    try:
        app = appdb[args.appname]
    except KeyError:
        print('ERROR: Unknown application "%s"' % args.appname)
        exit(1)
    if not args.quiet:
        print('appdb application entry: %s' % args.appname)
    for key, value in app.items():
        # indent continuation lines
        if isinstance(value, list):
            value = '\n'.join(value)
        value = re.sub(r'\n', r'\n    ', str(value))
        print('%s = %s' % (key, value))

def cmd_current_application(args):
    app = current_application(args)
    if not args.quiet:
        print('Current application: %s' % app.name)
    else:
        print(app.name)

def cmd_application_path(args):
    app_path = current_application(args).probe_path()
    if not args.quiet:
        print('Application path: %s' % app_path)
    else:
        print(app_path)



# Simulation actions

def cmd_create(args):
    try:
        # TODO: Add context "specify one with --parfile <file>" to
        # missing parameter file error

        # TODO: Add context "specify with --cores <ncores>" to missing
        # core/process count error

        pt_request = {'cores': args.cores,
                      'processes': args.processes,
                      'cores-per-process' : args.cores_per_process,
                      'processes-per-node' : args.processes_per_node,
                      'node-list' : args.nodelist}

        # argparse gives unspecified options as None; we just remove
        # them from the request
        pt_request = {k:v for k, v in pt_request.items() if v}

        machine = mdb.get_local_machine()

        sim = Simulation(
            args.simulation, current_build(args), args.parfile,
            machine,
            ParallelTopology(machine, pt_request),
            walltime=parse_walltime(args.walltime) if args.walltime else None,
            continuous_resubmission=args.continuous_resubmission,
            definitions=args.define,
            postsimulationscript=args.post_simulation_script)
        sim.activate()
        return sim

    except UserException as ex:
        ex.add_context(" while creating simulation "+args.simulation)
        raise




# Modify the simulation so that the next segment to be created will
# inherit the modifications. TODO: log these changes so that there is
# an audit trail.
def cmd_modify(args):
    machine = mdb.get_local_machine()
    simname = args.simulation
    sim = load_simulation(machine.name, simname)

    if sim.status() != 'P' and not args.force:
        print("Cannot modify a running or queued simulation")
        # TODO: actually, we should make this possible, so that
        # you can defer a change until the next segment is
        # started.  But at the moment, it might be a bit
        # dangerous.
        sys.exit(1)

    # TODO: try to figure out a way to warn the user if a certain
    # option is not supported. The following does not work, as args
    # contains a lot more than what has been given on the command
    # line.

    # supported = ["paralleltopology", "parfile", "build", "walltime", "define"]

    # for arg in vars(args):
    #     print(arg," = ",getattr(args,arg))
    #     if arg not in supported:
    #         print("Modification of "+arg+" is not supported")
    #         sys.exit(1)

    modifications = {}

    if args.cores:
        modifications['paralleltopology'] = ParallelTopology(
            machine, {'cores':args.cores})

    if args.parfile:
        modifications['parfile'] = args.parfile

    if args.build:
        print("Build modification not yet supported")
        sys.exit(1)

    if args.walltime:
        modifications['walltime'] = parse_walltime(args.walltime)

    if args.define:
        modifications['definitions'] = args.define
        # print("Definition modification not yet supported",file=sys.stderr)
        # sys.exit(1)

    if args.post_simulation_script:
        modifications['postsimulationscript'] = args.post_simulation_script

    sim.modify(modifications)

def cmd_clone(args):
    machine = mdb.get_local_machine()
    simfactory.simulation.clone(
        os.path.join(machine.eval('basedir'),args.origsim),
        args.newsim)

def check_no_mod(args, command):
    cannot_mod_msg = ('Cannot modify the {} an existing simulation using '
                      'sim {}.  Use sim modify instead.')
    if args.parfile != None:
        print(cannot_mod_msg.format("parameter file of", command))
        sys.exit(1)
    if args.cores != None:
        print(cannot_mod_msg.format("number of cores used by", command))
        sys.exit(1)
    if args.walltime != None:
        print(cannot_mod_msg.format("walltime of", command))
        sys.exit(1)

def cmd_submit(args):
    if not simulation_exists(mdb.get_local_machine().name, args.simulation):
        sim = cmd_create(args)
    else:
        print("Cannot use submit on existing simulation "+args.simulation+".  "
              "Use \"sim continue\" to continue a stopped simulation.", file=sys.stderr)
        sys.exit(1)
        # TODO: think about presubmission; the problem is that we want
        # to be able to presubmit several different simulations, but
        # the "submit" argument parser inherits from "create", which
        # can only take a single simulation.

        # check_no_mod(args, "submit")
        # sim = load_simulation(mdb.get_local_machine().name, args.simulation)

    sim.submit()

def cmd_continue(args):
    sims = load_sim_args(args.simulations)
    for sim in sims:
        sim.submit(continuation=args.force)

def cmd_run(args):
    if not simulation_exists(mdb.get_local_machine().name, args.simulation):
        sim = cmd_create(args)
    else:
        check_no_mod(args, "run")
        sim = load_simulation(mdb.get_local_machine().name, args.simulation)

    sim.run()

def cmd_stop(args):
    sims = load_sim_args(args.simulations)

    for sim in sims:
        sim.abort()

def cmd_pause(args):
    sims = load_sim_args(args.simulations)

    for sim in sims:
        sim.pause()

def cmd_update(args):
    sims = load_sim_args(args.simulations)

    for sim in sims:
        sim.refresh()

def cmd_deactivate(args):
    sims = load_sim_args(args.simulations)

    for sim in sims:
        sim.deactivate()

def cmd_activate(args):
    sims = load_sim_args(args.simulations)

    for sim in sims:
        sim.activate()

def print_table(table):
    if len(table) == 0:
        return

    widths = [0] * len(table[0])

    for row in table:
        these_widths = [len(x) for x in row]
        widths = [max(w, tw) for (w, tw) in zip(widths, these_widths)]

    for row in table:
        print("  ".join([("{:<"+str(w)+"}").format(s) for
                         (s, w) in zip(row, widths)]))

def glob_match(pattern, string):
#    return fnmatch.fnmatch(string, pattern)
    re_pattern = fnmatch.translate(pattern)
    # This only works in typical simple cases; nested braces will
    # break
    re_brace_pattern = r"\\\{.*?\\\}"
    new_pattern = re.sub(re_brace_pattern,
                         lambda m: '('+m.group(0)[2:-2].replace('\,', '|')+')',
                         re_pattern)
    return re.match(new_pattern, string)


def load_sim_args(patterns1):
    # TODO: tidy up the redundant machine-finding code.  Probably we
    # should be passing around just a machine.  Fix tests first.
    machine = mdb.get_local_machine()
    # TODO: should this be implemented as a default on args.simulations?
    patterns = patterns1 if len(patterns1) > 0 else ['*']

    # TODO: depending on the filesystem and the number of simulations,
    # this might be very slow.  In the case where the patterns are
    # all literal, we could skip this.
    allsimnames = simfactory.simulation.get_simulation_names(machine, '*')

#    print("allsimnames = ", allsimnames)

    simnames = sorted([simname for simname in allsimnames for pattern in
                       patterns if glob_match(pattern, simname)])

#    print("simnames =", simnames)

    # Flatten list
#    simnames = sorted(list(itertools.chain.from_iterable(simnames1)))
    sims = [load_simulation(machine.name, simname) for simname in simnames]
    return sims

def cmd_status(args):
    # if not args.quiet:
    #     print('Simulation status:')
    sims = load_sim_args(args.simulations)
    table = []
    # count=0
    for sim in sims:
        if not sim.active and not args.all:
            continue
        # count = count + 1
        # if count > 30:
        #     break
        stat = {'R': 'Running',
                'Q': 'Queued',
                'P': 'Stopped',
                'H': 'Held'}[sim.status()] if sim.active else 'Inactive'
        term_reason = (sim.termination_reason() if
                       stat == "Stopped" or not sim.active
                       else "")
#        progress = sim.progress()
        progress = None
        if not progress:
            progress = ("", "")
        cores = str(sim.paralleltopology.cores)
        mod_time = sim.last_output_time()
        table.append([sim.name, cores, stat, progress[0], progress[1],str(mod_time) if mod_time else "None",
                      term_reason])
        # print("\t".join(table[-1]))

    print_table(table)

class SimulationPropertyDict(dict):
    def __init__(self, sim):
        self.sim = sim

    def __missing__(self, key):
        # if key == 'name':
        #     return self.sim.name

        # if key == 'walltime':
        #     return str(self.sim.walltime)

        attrs = [str(s) for s in dir(self.sim)]

        if key in attrs:
            val = getattr(self.sim,key)
            if callable(val):
                return str(val())
            else:
                return str(val)

        raise KeyError(key)

def cmd_ls(args):
    # if not args.quiet:
    #     print('Simulation status:')
    sims = load_sim_args(args.simulations)
    table = []
    # count=0
    for sim in sims:
        if not sim.active:
            continue
        fmt = args.format
        props = SimulationPropertyDict(sim)
        print(fmt.format_map(props))

def setup_logging(verbose=False):
    # TODO: decide where this file should live, and note that
    # simfactory is not always started from sim.py, e.g. in the
    # simulation run script, and not always from the source base
    # directory
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        filename='simfactory.log',
        filemode='a')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO if verbose else logging.WARNING)
    formatter = logging.Formatter('%(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    logger.info('Simulation Factory %s', VERSION)

def setup_local_machine(args):
    if args.local_machinename:
        name = args.local_machinename
        try:
            mdb[name]
        except KeyError:
            raise UserException('Argument "%s" to option --machine '
                                'is not a known machine' % name)
        simfactory.mdb.set_local_machine(mdb[name])
    # If there is no --machine option passed,
    # simfactory.mdb.get_local_machine() will probe it automatically
    # when required

def setup_directory(args):
    if args.directory:
        logger.info('Switching to directory "%s"', args.directory)
        try:
            os.chdir(args.directory)
        except FileNotFoundError:
            raise UserException("Directory "+args.directory+" not found")

# Call main routine
if __name__ == '__main__':
#    cProfile.run('main()', 'sfprofile.txt')
    main()
