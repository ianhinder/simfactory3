import os
import shutil
import tempfile
import unittest

from simfactory.application import Application
from simfactory.utils import working_directory

class TestApplication(unittest.TestCase):

################################################################################

    def setUp(self):
        self.tempdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tempdir)
        pass # pylint: disable=unnecessary-pass

    def test_init(self):
        defs = {'checkpoint-pattern': '*chkpt.it_*',
                'fingerprint-file': 'arrangements',
                'executable': '{app-path}/exe/cactus_{build}',
                'default-build': 'sim',
                'runcmd': '{parallel-runprefix} {executable} -L 3 {par}',
                'sync-filter-rules': ['-p .git', '-p /exe', '-p doc', '-p /configs']}
        app = Application("myapp", defs)
        self.assertEqual(app['checkpoint-pattern'], '*chkpt.it_*')
        self.assertEqual(app['sync-filter-rules'], ['-p .git', '-p /exe', '-p doc', '-p /configs'])

    def test_probe_path(self):
        defs = {'fingerprint-file': 'arrangements'}
        app = Application("myapp", defs)
        app_path = os.path.abspath(os.path.join(self.tempdir,"MyApp"))
        os.mkdir(app_path)
        os.mkdir(os.path.join(app_path,"arrangements"))
        subdir = os.path.join(app_path,"subdir")
        os.mkdir(subdir)

        with working_directory(subdir):
            self.assertTrue(os.path.samefile(app.probe_path(), app_path))

    def test_recover(self):
        defs = {'checkpoint-pattern': '*chkpt.it_*'}
        app = Application("myapp", defs)

        s1 = os.path.join(self.tempdir,'output-0000')
        s2 = os.path.join(self.tempdir,'output-0001')

        for s in [s1,s2]:
            os.mkdir(s)

        name = "checkpoint.chkpt.it_0"
        _empty_file(os.path.join(s1,name))

        app.prepare_recovery([s1], s2)

        self.assertTrue(os.path.samefile(os.path.join(s1,name), os.path.join(s2,name)))

    def test_recover_lastonly(self):
        defs = {'checkpoint-pattern': '*chkpt.it_*'}
        app = Application("myapp", defs)

        s1 = os.path.join(self.tempdir,'output-0000')
        s2 = os.path.join(self.tempdir,'output-0001')
        s3 = os.path.join(self.tempdir,'output-0002')

        for s in [s1,s2,s3]:
            os.mkdir(s)

        name = "checkpoint.chkpt.it_0"
        _empty_file(os.path.join(s1,name))
        _empty_file(os.path.join(s2,name))

        app.prepare_recovery([s1, s2], s3)

        self.assertTrue(os.path.samefile(os.path.join(s2,name), os.path.join(s3,name)))
        self.assertFalse(os.path.samefile(os.path.join(s1,name), os.path.join(s3,name)))

    def test_recover_skip(self):
        defs = {'checkpoint-pattern': '*chkpt.it_*'}
        app = Application("myapp", defs)

        s1 = os.path.join(self.tempdir,'output-0000')
        s2 = os.path.join(self.tempdir,'output-0001')
        s3 = os.path.join(self.tempdir,'output-0002')

        for s in [s1,s2,s3]:
            os.mkdir(s)

        name = "checkpoint.chkpt.it_0"
        _empty_file(os.path.join(s1,name))

        app.prepare_recovery([s1, s2], s3)

        self.assertTrue(os.path.samefile(os.path.join(s1,name), os.path.join(s3,name)))

    def test_get_checkpoint_files(self):
        defs = {'checkpoint-pattern': '*chkpt.it_*'}
        app = Application("myapp", defs)

        s1 = os.path.join(self.tempdir,'output-0000')

        os.mkdir(s1)

        filenames = ["checkpoint.chkpt.it_{}".format(i) for i in range(0,4)]

        for filename in filenames:
            _empty_file(os.path.join(s1,filename))

        found = app.get_checkpoint_files(s1)
        self.assertEqual(sorted(found), sorted(filenames))

################################################################################

def _empty_file(name):
    if os.path.exists(name):
        raise Exception("File {} exists".format(name))
    with open(name, 'a'):
        pass

################################################################################

if __name__ == '__main__':
    unittest.main(verbosity=2)
