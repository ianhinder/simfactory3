
################################################################################

"""
===================
Simple Batch System
===================

The Simple Batch System is a toy batch queuing system.
"""

################################################################################

import os
import re
import subprocess
import stat
import argparse
import signal
import sys

################################################################################

################################################################################
# Public functions
################################################################################

def submit(command, rundir=".", out="job.out", err="job.err"):
    """Run a command in the background as a job.  Return a job id to
    refer to this process.  out and err are basenames for the output
    and error files"""

    if not os.path.exists(rundir):
        os.mkdir(rundir)
    with open(os.path.join(rundir,out),"w") as o, open(os.path.join(rundir,err),"w") as e:
        popen = subprocess.Popen(command, stdin=None,stdout=o,stderr=e,
                                 close_fds=True,cwd=rundir,
                                 start_new_session=True)
        pid = popen.pid

    job_id = str(pid)
    return job_id

def stop(job_id):
    "Stop a running job"
    os.killpg(int(job_id),signal.SIGKILL)

def status(job_id):
    """Return the job status as a string. R means it is running, U
    means it is not found, so is probably finished"""
    if _is_process_running(int(job_id)):
        return 'R'
    else:
        return 'U'

def _is_process_running(pid):
    try:
        os.kill(pid,0)
        return True
    except OSError:
        return False

def cmd_sub(args):
    print("Submitted", submit(args.script,out=args.stdout,err=args.stderr))
    with open('WALLTIME', 'w') as f:
        f.write(args.walltime)

def cmd_stat(args):
    s = status(args.jobid)
    if s == 'U':
        print("Job", args.jobid, " is not known")
        sys.exit(1)
    print(args.jobid, s)

def cmd_del(args):
    stop(args.jobid)

parser = argparse.ArgumentParser()

subparsers = parser.add_subparsers(
    help='must be one of the following subcommands:', metavar='command')

# Submit
parser_sub = subparsers.add_parser('sub', help='submit a job (it will run in the current working directory)')
parser_sub.set_defaults(func=cmd_sub)
parser_sub.add_argument('script', help='script file')
parser_sub.add_argument(
    '--stdout', help='output file location', required=False, default="job.out")
parser_sub.add_argument(
    '--stderr', help='error file location', required=False, default="job.err")
parser_sub.add_argument(
    '--walltime', help='time to run as h:m:s', required=False, default="24:00:00")

# Status
parser_stat = subparsers.add_parser('stat', help='get the status of a job')
parser_stat.set_defaults(func=cmd_stat)
parser_stat.add_argument('jobid', help='job id')

# Delete
parser_del = subparsers.add_parser('del', help='stop a job')
parser_del.set_defaults(func=cmd_del)
parser_del.add_argument('jobid', help='job id')

################################################################################
# Main
################################################################################

args = parser.parse_args()
args.func(args)
