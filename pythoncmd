#!/usr/bin/env python

# Find the local machine from the mdb using portable python, so that
# the runpython script can find the python3 for this machine.  We
# assume that all machines have at least a Python 2.  If not, this
# could be rewritten in bash.

import glob
import os
import re
import socket
import sys

def load_machine_def(filename):
    f = open(filename, "r")

    hostnamepattern = None
    machinename = None
    pythoncmd = "python3" # This is hopefully a good default; if we
                          # have a minimum version of python 3 that is
                          # supported, this will have to change

    for line in f:
        match = re.match('[ ]*hostnamepattern:[ \t]*(.*)\n', line)
        if match:
            hostnamepattern = match.group(1)

        match = re.match('[ ]*name:[ ]*(.*)\n', line)
        if match:
            machinename = match.group(1)

        match = re.match('[ ]*pythoncmd:[ ]*(.*)\n', line)
        if match:
            pythoncmd = match.group(1)

    return {'name' : machinename, 'pythoncmd' : pythoncmd,
            'hostnamepattern' : hostnamepattern}

def local_machine_def():
    sfdir=os.path.dirname(os.path.realpath(__file__))
    machinefilenames = glob.glob(sfdir+"/simfactory/etc/mdb/*.yaml")
    if len(machinefilenames) == 0:
        sys.stderr.write("Cannot find machine definition files\n")
        sys.exit(1)
    machines = [load_machine_def(f) for f in machinefilenames]
    hostname = socket.gethostname()
    candidates = []
    if 'SIMFACTORY_MACHINE' in os.environ:
        name = os.environ['SIMFACTORY_MACHINE']
        # sys.stderr.write("Found SIMFACTORY_MACHINE="+name+"\n")
        # sys.stderr.write("machine names = "+str([m['name'] for m in machines])+"\n")
        candidates = [m for m in machines if m['name'] == name]
    else:
        # sys.stderr.write("Looking for matching hostname\n")
        candidates = [m for m in machines if re.search(m['hostnamepattern'], 
                                                       hostname)]

    if not candidates:
        sys.stderr.write("Unrecognised local machine '%s'\n" % hostname)
        sys.exit(1)
    elif len(candidates) > 1:
        sys.stderr.write("Ambiguous local machine, candidates are "+str([m['name'] for m in candidates])+"\n")
        sys.exit(1)
    else:
        return candidates[0]

print(local_machine_def()['pythoncmd'])
