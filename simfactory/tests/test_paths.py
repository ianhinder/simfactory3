
import unittest
import os

import simfactory.paths

class TestPaths(unittest.TestCase):

    def test_basepath(self):
        self.assertTrue(os.path.exists(
            os.path.join(simfactory.paths.basepath(),'simfactory/simulation.py')))

    def test_libpath(self):
        self.assertTrue(os.path.exists(
            os.path.join(simfactory.paths.libpath(),'simulation.py')))

    def test_etcpath(self):
        self.assertTrue(os.path.exists(
            os.path.join(simfactory.paths.etcpath(),'mdb')))

    def test_appdbpath(self):
        self.assertTrue(os.path.exists(
            os.path.join(simfactory.paths.appdbpath(),'generic.yaml')))

    def test_mdbpath(self):
        self.assertTrue(os.path.exists(
            os.path.join(simfactory.paths.mdbpath(),'datura.yaml')))

    def test_testspath(self):
        self.assertTrue(os.path.exists(
            os.path.join(simfactory.paths.testspath(),'test_simulation.py')))

if __name__ == '__main__':
    unittest.main(verbosity=2)
