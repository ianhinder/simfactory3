# SimFactory 3 #

External links:

* [SimFactory 2](simfactory.org) - The origin of and motivation for SimFactory
* [A talk about SimFactory 3](https://ianhinder.net/talks/2016.06-Hinder-ET_workshop-The_Future_of_SimFactory.pdf)
* [Continuous integration tests](https://build.barrywardell.net/view/All/job/SimFactory3)

### Quickstart guide ###

(still very rough; work in progress)

* Download and compile Cactus as normal using either "make" or SimFactory 2; SimFactory 3 does not yet support compiling.
* Install SimFactory3 in your Cactus directory:
```
#!bash
    cd Cactus
    git clone git@bitbucket.org:ianhinder/simfactory3.git
    alias sim3=simfactory3/sim

```
* Make sure that Python 3 is available.  On supported machines, SimFactory will read the python location from the machine.yaml file.  Otherwise, it will look in various places and on the PATH; see simfactory3/runpython.
* Create a personal configuration file in simfactory3/simfactory.yaml with your personal options for each machine, e.g.
```
    Machine:
      minerva:
        sourcebasedir:  /home/ianhin/Cactus
        basedir:        /scratch/ianhin/simulations/
        user:           ianhin
```
* Run a test simulation:
```
    sim3 submit qc0-mclachlan --parfile par/qc0-mclachlan.par --cores 32 --walltime 24:00:00
```
* To add a new machine, copy one of the existing machine definitions, e.g. simfactory3/simfactory/etc/mdb/minerva.yaml, and edit it to match your machine.