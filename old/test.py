# import Configuration

# print "Configurations: ", Configuration.get_configurations()

# c = Configuration.Configuration("sim")
# c.delete()

# c.build(thornlist="simfactory/etc/thornlists/basic.th",
#         options="simfactory/mdb/optionlists/macos-fink-gcc.cfg")


# import ConfigFile

# c = ConfigFile.ConfigFile("test.ini")
# c['hello'] = "Hello World!"

# print c['hello']

import SimConfiguration
c = SimConfiguration.SimConfiguration("sim")
#c['optionlist'] = "simfactory/mdb/optionlists/macos-fink-gcc.cfg"
c['thornlist'] = "simfactory/etc/thornlists/basic.th"
#print c['optionlist']
print c['thornlist']
#print c.keys()
c['thornlist'] = "simfactory/etc/thornlists/basic.th"

import MachineDB
import SimConfiguration
import ConfigParser

mdb = MachineDB.MachineDB('/Users/ian/Projects/simfactory3/mdb/machines')

# for m in mdb.list_machines():
#     print m

# print mdb.get_machine('damiana')

damiana = mdb.get_machine('damiana')
c = SimConfiguration.SimConfiguration("sim", machine=damiana)

try:
    print "In config:",c['optionlist']
except KeyError:
    print "optionlist not in config"


print "Used:",c.get_used_optionlist()
