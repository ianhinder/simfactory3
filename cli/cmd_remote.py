"""
Command-line subcommands for remote access
"""

import os
import subprocess

from simfactory.mdb import mdb
from simfactory.utils import quote, convert_to_remote_dir

from cli.utils import local_machine, current_application



def cmd_execute(args):
    """
    Execute a shell command
    """
    lm = local_machine(args.local_machinename)
    cmd = lm['envsetup'] + "\n" + ' '.join([quote(arg) for arg in args.arguments])
    if args.verbose:
        print('Executing command: %s' % cmd)
    subprocess.call(cmd, shell=True)

def cmd_login(args):
    """
    Call an interactive shell
    """
    if not args.quiet:
        print('Starting shell...')
    subprocess.call('bash -i -l', shell=True)
    if not args.quiet:
        print('Shell finished.')

def _get_sshprefix(lm, rm):
    """
    Determine ssh prefix to connect from lm to rm
    """
    if rm.name == lm.name:
        raise Exception('Refusing to ssh from a machine to itself')
    rsshvariant = rm['sshvariant']
    if rsshvariant=='ssh':
        lsshcmd = lm['sshcmd']
    elif rsshvariant=='gsissh':
        lsshcmd = lm['gsisshcmd']
    else:
        raise Exception('Illegal value "%s" for key "sshvariant" '
                        'for machine "%s"' % (rsshvariant, rm.name))
    rsshopts = rm['remotesshopts']
    lsshopts = lm['localsshopts']
    ruser = rm['user']
    rhostname = rm['hostname']
    # if rsshvariant=='gsissh':
    #     # TODO: Set up gsissh credentials
    #     raise Exception('gsissh not yet supported')
    return (' '.join([lsshcmd, rsshopts, lsshopts]), ruser+'@'+rhostname)

def cmd_remote(args):
    """
    Forward the Simfactory invocation to a remote machine
    """
    lm = local_machine(args.local_machinename)
    # TODO: handle multiple remote machines?
    rm = mdb[args.machinename]
    lpath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    rpath = convert_to_remote_dir(lm, rm, lpath)
    ldir = os.getcwd()
    rdir = convert_to_remote_dir(lm, rm, ldir)
    if args.verbose:
        print('Executing on remote machine %s:' % rm.name)
    cmd = ' '.join([quote(os.path.join(rpath, 'sim')),
                    '--machine', rm.name,
                    '--directory', quote(rdir)] +
                   [quote(arg) for arg in args.arguments])
    # Create a connection from lm to dm
    dm = rm
    while dm.name != lm.name:
        # Can we connect from lm to dm directly, or do we need a
        # trampoline?
        if 'trampoline' in dm:
            tm = mdb[dm['trampoline']]
        else:
            tm = lm
        # Connect from tm to dm
        sshprefix = ' '.join(_get_sshprefix(tm, dm))
        cmd = ' '.join([sshprefix, quote(cmd)])
        # Handle remainder, i.e. the connection from lm to tm
        dm = tm
    if args.verbose:
        print('Executing command: %s' % cmd)
    subprocess.call(cmd, shell=True)

def cmd_sync(args):
    """
    Synchronise the source directory tree to a remote machine
    """
    lm = local_machine(args.local_machinename)
    app = current_application(args)
    ldir = app.probe_path()
    for rm in [mdb[name] for name in args.machinename]:
        if rm.name == lm.name:
            if not args.quiet:
                print('Not syncing to remote machine "%s", '
                      'since it is identical to the local machine' % rm.name)
            continue
        # TODO: create rdir and its parents if it does not exist
        rdir = convert_to_remote_dir(lm, rm, ldir)
        lrsynccmd = lm['rsynccmd']
        rrsynccmd = rm['rsynccmd']
        lrsyncopts = lm['localrsyncopts']
        rrsyncopts = rm['remotersyncopts']
        # TODO: move these into the MDB defaults?
        fixedopts = ' '.join(
            # TODO: --delete deletes files in the Cactus directory,
            # which shouldn't happen
            ['--archive', '--hard-links', '--sparse', # '--delete'
             '--compress',
             '--partial', '--progress', '--stats'])
        verboseopts = ''
        if args.verbose:
            verboseopts = ' '.join(['--verbose'])
        filter_rules = app['sync-filter-rules']
        filters = ' '.join([quote('--filter='+p) for p in filter_rules])
        if not args.quiet:
            print('Syncing to remote machine %s...' % rm.name)
        # Create a connection from lm to dm
        # This is similar as for cmd_remote, except that we need to
        # split off the last sshaccess, so that we can use it as path
        # prefix.
        cmd = None
        last_sshaccess = None
        dm = rm
        while dm.name != lm.name:
            # Can we connect from lm to dm directly, or do we need a
            # trampoline?
            if 'trampoline' in dm:
                tm = mdb[dm['trampoline']]
            else:
                tm = lm
            # Connect from tm to dm
            (sshprefix, sshaccess) = _get_sshprefix(tm, dm)
            if cmd:
                cmd = ' '.join([sshprefix, sshaccess, quote(cmd)])
            else:
                cmd = sshprefix
                last_sshaccess = sshaccess
            # Handle remainder, i.e. the connection from lm to tm
            dm = tm
        if cmd is None or last_sshaccess is None:
            raise Exception('internal error')
        (sshprefix, sshaccess) = (cmd, last_sshaccess)
        cmd = ' '.join([lrsynccmd, fixedopts, verboseopts,
                        filters, rrsyncopts, lrsyncopts,
                        quote('--rsh='+sshprefix),
                        quote('--rsync-path='+rrsynccmd),
                        quote(ldir+'/'), quote(sshaccess+':'+rdir+'/')])
        if args.verbose:
            print('Executing command: %s' % cmd)
        subprocess.call(cmd, shell=True)
