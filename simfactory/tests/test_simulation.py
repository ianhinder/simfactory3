import filecmp
import os
import shutil
import tempfile
import time
import unittest

from simfactory import simulation
from simfactory.mdb import mdb
from simfactory.paths import testspath
from simfactory.build import Build
from simfactory.simulation import Simulation
from simfactory.paralleltopology import ParallelTopology
from simfactory.utils import wait_for
import simfactory.appdb

class TestSimulation(unittest.TestCase):

################################################################################

    def setUp(self):
        # create a temporary working directory
        self.machine = mdb.probe_local_machine()

        self.assertTrue(os.path.exists(self.machine.eval('basedir')))
        testsdir = os.path.join(self.machine.eval('basedir'), "tests")
        if not os.path.exists(testsdir):
            os.mkdir(testsdir)
        self.tempdir = tempfile.mkdtemp(dir=testsdir)
#        print("tempdir = "+self.tempdir)
        self.simname = self.id().split(".")[-1]
        self.simdir = self.tempdir+"/"+self.simname
        self.appdb = simfactory.appdb.ApplicationDB(
            [os.path.join(simfactory.paths.testspath(), 'testappdb/appdb.yaml')], [])

    def tearDown(self):
        shutil.rmtree(self.tempdir)

    def new_simulation(self, name=None, apptype="finish"):
        if name == None:
            name = self.simname
        exe = {"finish" : os.path.abspath(testspath()+"/testapp/testapp.sh"),
               "wait" : os.path.abspath(testspath()+"/testapp/testwaitapp.sh"),
               "checkpoint" : os.path.abspath(testspath()+"/testapp/testrecoverapp.sh")}[apptype]

        sim = Simulation(name, Build("generic", self.appdb['generic'], '', exe),
                         testspath()+"/testapp/test.par",
                         self.machine,
                         ParallelTopology(self.machine, {'nodes': 1}),
                         walltime=simfactory.utils.parse_walltime('00:05:00'),
                         basedir=self.tempdir)
        self.check_properties(self.tempdir+"/"+name)
        return sim

################################################################################

    def check_output_and_error(self, prefix=None):
        if prefix == None:
            prefix = os.path.join(self.simdir, simulation.segment_name(0), self.simname)
        with open(prefix+".out", "r") as out, open(prefix+".err", "r") as err:
            self.assertEqual(out.readlines()[-2:],
                             ['Test application running\n', 'variable = "value"\n'])
            self.assertEqual(prune_error(err.readlines()), ['This is a warning\n'])

    def check_properties(self, simdir):
        self.assertTrue(os.path.exists(simdir+"/SIMFACTORY/properties.py"))

    def wait_for_file(self, filename, what, errfile=None):
        # TODO: handle timeout more accurately

        # TODO: what to do about timeouts when waiting for a job to
        # start?  Probably need to wait first for the job to be no
        # longer queued, then apply the standard timeout

        timeout = 3600*12 # Wait 12 hours for the job to run
        period = 0.1
        tries = int(timeout/period)
        while not os.path.exists(filename) and tries > 0:
            time.sleep(period)
            tries -= 1
            if errfile and os.path.exists(errfile) and os.path.getsize(errfile) > 0:
                # TODO: the error file might not have been fully written
                errmsg = _file_content(errfile)
                self.assertFalse(
                    simfactory.utils.file_matches_pattern(
                        errfile, 'Error|Traceback'),
                    "Error file non-empty: "+"\n".join(errmsg))

        self.assertNotEqual(
            tries, 0,
            msg="Timed out waiting {0}s for test application to {1}".format(
                timeout, what))

    def wait_for_status(self, sim, state):
        # TODO: handle timeout more accurately
        timeout = 500
        period = 0.1
        tries = int(timeout/period)
        while sim.status() != state and tries > 0:
            time.sleep(0.1)
            tries -= 1
        self.assertNotEqual(
            tries, 0,
            msg="Timed out waiting {0}s for job status {1}".format(timeout, state))


################################################################################

    def test_new(self):
        sim = self.new_simulation()
        self.assertFalse(sim.active)

    def test_par(self):
        sim = self.new_simulation()
        simparname = os.path.join(sim.dir, "SIMFACTORY/par/test.par")
        self.assertTrue(os.path.samefile(sim.par, simparname))
        self.assertTrue(filecmp.cmp(simparname,
                                    os.path.join(testspath(), "testapp", "test.par"),
                                    shallow=False))

    def test_exe(self):
        sim = self.new_simulation()
        self.assertTrue(os.path.samefile(
            sim.build.exe, os.path.join(sim.dir, "SIMFACTORY/build/testapp.sh")))
        self.assertTrue(
            filecmp.cmp(sim.build.exe,
                        os.path.join(testspath(), "testapp", "testapp.sh"),
                        shallow=False))

    def test_load(self):
        sim1 = self.new_simulation()
        sim2 = simulation.load(self.simdir)
        self.assertEqual(vars(sim1), vars(sim2))

    def test_activate(self):
        sim = self.new_simulation()
        sim.activate()
        self.assertTrue(sim.active)

    def test_run(self):
        sim = self.new_simulation()
        sim.activate()
        with open(self.tempdir+"/test.out", "wb") as out, open(self.tempdir+"/test.err", "wb") as err:
            self.assertEqual(sim.run(stdout=out, stderr=err), 0)
        self.check_output_and_error(self.tempdir+"/test")
        self.assertTrue(os.path.exists(
            os.path.join(self.simdir, simulation.segment_name(0), "done")))

    def test_submit(self):
        sim = self.new_simulation()
        sim.activate()
        sim.submit()
        self.wait_for_file(os.path.join(self.simdir, simulation.segment_name(0), "done"),
                           "finish",
                           os.path.join(self.simdir, simulation.segment_name(0),
                                        self.simname+".err"))
        self.check_output_and_error()
        # TODO: test exit code; how do we get this?  Need some way to
        # store it in the output directory and retrieve it later


# TODO: SGE on Datura kills the process immediately without sending a
# signal which can be caught and used to write the "stopped" file.  So
# we cannot determine whether the job has stopped except by using the
# status method, and this is tested already in test_status.  The only
# option I can think of is to have the test application periodically
# write to a file, and we can test that the file stops being written
# to.

    # def test_stop(self):
    #     s = self.new_simulation(apptype="wait")
    #     s.activate()
    #     self.wait_for_file(self.simdir+"/waiting", "initialse")
    #     self.check_output_and_error()
    #     s.stop()
    #     self.wait_for_file(self.simdir+"/stopped", "stop")

    def test_status(self):
        s = self.new_simulation(apptype="wait")
        s.activate()
        s.submit()
        self.wait_for_file(
            os.path.join(self.simdir,simulation.segment_name(0),"waiting"),
            "initialise",
            os.path.join(self.simdir,simulation.segment_name(0),self.simname+".err"))
        self.check_output_and_error()
        self.assertEqual(s.status(), 'R')
        s.abort()
        self.wait_for_status(s, 'P')

    def test_recovery(self):
        s = self.new_simulation(apptype="checkpoint")
        # TODO: make a separate app type for testing just recovery
        # with no continuous resubmission
        s.continuous_resubmission = False
        prefix=os.path.join(self.simdir,simulation.segment_name(0),self.simname)

        s.activate()
        s.submit()

        while s.status() != 'P':
            time.sleep(0.1)

        cp0 = os.path.join(self.simdir,simulation.segment_name(0),"checkpoint-1")
        out0=os.path.join(self.simdir,simulation.segment_name(0),self.simname+".out")
        err0=os.path.join(self.simdir,simulation.segment_name(0),self.simname+".err")

        # The job can be finished, but filesystem changes may not have
        # propagated.  Therefore keep looking for the file until a
        # timeout of 1s.
        self.wait_for_file(cp0,"checkpoint", err0)
        self.assertEqual(_file_content(cp0), ['1\n'])

        self.assertEqual(prune_error(_file_content(err0)),
                         [])



        outcontent = _file_content(out0)

        self.assertIn('Checkpoint test application running\n', outcontent)
        self.assertIn('No checkpoint files found\n', outcontent)
        self.assertIn('Dumping checkpoint at iteration {}\n'.format(1), outcontent)
        self.assertIn('Terminated due to walltime\n', outcontent)

        s.submit()

        while s.status() != 'P':
            time.sleep(0.1)

        out1=os.path.join(self.simdir,simulation.segment_name(1),self.simname+".out")
        err1=os.path.join(self.simdir,simulation.segment_name(0),self.simname+".err")
        cp1 = os.path.join(self.simdir,simulation.segment_name(1),"checkpoint-2")
        self.wait_for_file(cp1,"checkpoint",err1)
        self.assertEqual(_file_content(cp1), ['2\n'])

        self.assertEqual(prune_error(_file_content(err1)),
                         [])

        outcontent = _file_content(out1)

        self.assertIn('Checkpoint test application running\n', outcontent)
        self.assertIn('Recovered from iteration {}\n'.format(1), outcontent)
        self.assertIn('Dumping checkpoint at iteration {}\n'.format(2), outcontent)
        self.assertIn('Terminated due to walltime\n', outcontent)

    def test_continuous_resubmission(self):
        s = Simulation(self.simname,
                       Build("generic",self.appdb['generic'],'',
                             os.path.abspath(testspath()+
                                             "/testapp/testrecoverapp.sh")),
                       testspath()+"/testapp/test.par",
                       self.machine,
                       ParallelTopology(self.machine,{'cores': 12}),
                       basedir=self.tempdir,
                       continuous_resubmission=True)
        simdir = self.tempdir+"/"+self.simname
        self.check_properties(simdir)

        prefix=os.path.join(self.simdir,simulation.segment_name(0),self.simname)

        s.activate()
        s.submit()

        # TODO: could get corrupted pickle file here.  Need to
        # implement locking.

        counter = 0
        maxcount = 10

        out1 = os.path.join(self.simdir,simulation.segment_name(1),self.simname+".out")
        out2 = os.path.join(self.simdir,simulation.segment_name(2),self.simname+".out")

        err0 = os.path.join(self.simdir,simulation.segment_name(0),self.simname+".err")
        err1 = os.path.join(self.simdir,simulation.segment_name(1),self.simname+".err")
        err2 = os.path.join(self.simdir,simulation.segment_name(2),self.simname+".err")

        self.wait_for_file(out1,"output",err0)
        self.wait_for_file(out2,"output",err1)

        while len(s.segments) < 3 and counter < maxcount:
            s = simulation.load(simdir)
            counter=counter+1
            time.sleep(1)

        s.abort()

        for i in range(0,2):
            err=os.path.join(self.simdir,simulation.segment_name(i),self.simname+".err")
            if os.path.exists(err):
                errmsg = _file_content(err)
                self.assertTrue(prune_error(errmsg) == [],"Error running simulation: "+"\n".join(errmsg))

        self.assertTrue(counter < maxcount, "Timed out while waiting for 3 simulation segments to be created")

        # We cannot have a timeout here because we might be waiting in
        # a queueing system
        while s.status() != 'P':
            time.sleep(0.1)

        for i in range(0,2):
            cp = os.path.join(self.simdir,simulation.segment_name(i),"checkpoint-{}".format(i+1))
            self.assertTrue(wait_for(lambda:os.path.exists(cp),timeout=10))
            self.assertEqual(_file_content(cp), ['{}\n'.format(i+1)])

            out=os.path.join(self.simdir,simulation.segment_name(i),self.simname+".out")
            err=os.path.join(self.simdir,simulation.segment_name(i),self.simname+".err")
            self.assertEqual(prune_error(_file_content(err)),
                             [])
 
            outcontent = _file_content(out)
            self.assertIn('Checkpoint test application running\n', outcontent)

            if i==0:
                self.assertIn('No checkpoint files found\n', outcontent)
            else:
                self.assertIn('Recovered from iteration {}\n'.format(i), outcontent)

            self.assertIn('Dumping checkpoint at iteration {}\n'.format(i+1), outcontent)

            if i==2:
                self.assertIn('Terminated due to walltime\n', outcontent)
            else:
                self.assertIn('Terminated due to walltime\n', outcontent)

                


################################################################################

def _file_content(name):
    with open(name, "r") as o:
        return o.readlines()

def prune_error(l):
    return [s for s in l if s != 'Coverage.py warning: No data was collected.\n']

################################################################################


if __name__ == '__main__':
    unittest.main(verbosity=2)
