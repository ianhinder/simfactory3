""" SimFactory - The Simulation Factory

SimFactory is a Python package to manage simulations.

"""

__version__ = "3.0"
