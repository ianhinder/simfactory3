import unittest

import simfactory.paralleltopology
from simfactory.paralleltopology import ParallelTopology
from simfactory.mdb import mdb

#from pprint import pprint

class TestParallelTopology(unittest.TestCase):

################################################################################

    def setUp(self):
        self.machine = mdb.probe_local_machine()

    def test_empty(self):
        req = {}
        self.assertRaises(simfactory.paralleltopology.MissingCoreProcessError,
                          ParallelTopology, self.machine, req)

    def test_both_core_process(self):
        req = {'cores': 1, 'processes': 2}
        self.assertRaises(simfactory.paralleltopology.CoreProcessError,
                          ParallelTopology, self.machine, req)

    def test_only_cores(self):
        req = {'cores': 12}
        ptop = ParallelTopology(self.machine, req)
        self.assertEqual(ptop.cores, 12)

    def test_only_processes(self):
        req = {'processes': 3}
        ptop = ParallelTopology(self.machine, req)
        self.assertEqual(ptop.processes, 3)

    def test_one_node(self):
        req = {'processes-per-node' : 2,
             'cores-per-process': 6,
             'threads-per-core':1,

             'cores': 12}

        ptop = ParallelTopology(self.machine, req)

#        print(pprint(vars(p)))

        self.assertEqual(ptop.processes, 2)
        self.assertEqual(ptop.cores, 12)
        self.assertEqual(ptop.nodes, 1)
        self.assertEqual(ptop.threads_per_process, 6)

    def test_two_nodes(self):
        req = {'processes-per-node' : 2,
             'cores-per-process': 6,
             'threads-per-core': 1,
             
             'cores': 24}

        ptop = ParallelTopology(self.machine, req)

        self.assertEqual(ptop.processes, 4)
        self.assertEqual(ptop.cores, 24)
        self.assertEqual(ptop.nodes, 2)
        self.assertEqual(ptop.threads_per_process, 6)

################################################################################

if __name__ == '__main__':
    unittest.main(verbosity=2)
