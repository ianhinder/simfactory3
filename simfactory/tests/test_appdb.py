import unittest
import glob, os

import simfactory.paths
from simfactory import application
from simfactory import appdb

class TestApplicationDB(unittest.TestCase):

    def setUp(self):
        self.appdb_yamls = glob.glob(os.path.join(simfactory.paths.testspath(), 'testappdb/appdb*.yaml'))
        self.udb_yaml = os.path.join(simfactory.paths.testspath(), 'testappdb/udb.yaml')

        self.assertTrue(len(self.appdb_yamls) > 0)
        self.assertTrue(os.path.exists(self.udb_yaml))

        self.appdb = appdb.ApplicationDB(self.appdb_yamls, [self.udb_yaml])

    def test_list_apps(self):
        # TODO: this seems a strange way to test the keys
        self.assertEqual(self.appdb.keys(), dict({'test': None, 'other': None, 'generic': None}).keys())

    def test_have_app(self):
        self.assertTrue('test' in self.appdb)
        self.assertFalse('test2' in self.appdb)

    def test_get_app(self):
        a = self.appdb['test']
        self.maxDiff = None
        self.assertEqual(sorted(list(a._defs.items())), sorted(list(application.Application('test',
            {'executable': 'testexe',
             'name': 'test',
             'checkpoint-pattern': "*.chk.*"})._defs.items())))

    def test_appdb(self):
        '''Check all apps in the database are valid'''
        bad_apps = []
        for a in appdb.appdb:
            try:
                appdb.appdb[a]
            except (KeyError, ValueError) as e:
                bad_apps += [(a, e.args[0])]
        self.assertListEqual([], bad_apps)

if __name__ == '__main__':
    unittest.main(verbosity=2)
