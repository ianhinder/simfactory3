"""
============
paths module
============

The paths module contains functions for locating parts of simfactory.
"""

import os

# ICH: The naming could be "path" or "dir"; not sure
# BW: I don't think it makes sense to have basepath or testspath defined here
#     as the break encapsulation and encourage the low-level package to call
#     the high-level commands

# ICH: As far as I can tell, __file__ is expanded on import of the
# module, not when the code in the below functions is actually run. If
# the CWD is under sys.path, it will be expanded to a relative path,
# which may become wrong if the CWD subsequently changes.  Hence, we
# cache the absolute path when the module is first imported.

_MODPATH = os.path.abspath(__file__)

def basepath():
    """Return the path to the base simfactory directory (i.e. the
    top-level repository directory)"""
    return os.path.dirname(os.path.dirname(_MODPATH))

def libpath():
    """Return the path to the simfactory library directory; the one
    containing the modules"""
    return os.path.join(basepath(), "simfactory")

def etcpath():
    """Return the path to the simfactory library configuration
    directory"""
    return os.path.join(libpath(), "etc")

def appdbpath():
    """Return the path to the built-in application database"""
    return os.path.join(etcpath(), "appdb")

def mdbpath():
    """Return the path to the built-in machine database"""
    return os.path.join(etcpath(), "mdb")

def testspath():
    """Return the path to the directory containing simfactory tests
    and test data"""
    return os.path.join(libpath(), "tests")
