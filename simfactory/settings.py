"""
==========
settings module
==========

The settings module contains the settings object which represents a database of settings.

The settings module contains the settings object which represents a database of settings; this
acts as a dict, and loads its values from configuration files.  These files are validated against
a schema of recognised settings.
"""

import os
import simfactory.utils
import simfactory.yaml as yaml

from simfactory.optiondb import OptionDBEntry
import simfactory.paths

__all__ = ['settings']

class SettingsDB(OptionDBEntry):
    """
    Settings database object.
    """

    SCHEMA = {
        'email': {
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]+@[a-zA-Z0-9_+-.]+',
            'description': "User's email address",
            'example': 'sombody@somewhere.com'}}

_SETTINGS_FILES = [os.path.expanduser('~/.simfactory.yaml'),
                   os.path.join(simfactory.paths.basepath(), 'simfactory.yaml')]

def load_settings(files):
    settings_acc = {}
    for filename in files:
        try:
            with open(filename) as settings_file:
                file_obj = yaml.load(settings_file)
                if 'Settings' in file_obj and file_obj['Settings']:
                    # TODO: check settings against a schema
                    settings_acc = simfactory.utils.merge_dicts(settings_acc,
                                                                file_obj['Settings'])
        except IOError:
            # Ignore files which can't be read
            pass
    return SettingsDB("settings", settings_acc)

settings = load_settings(_SETTINGS_FILES)
