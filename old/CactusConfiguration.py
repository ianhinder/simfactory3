
"""
===========================
CactusConfiguration Module
===========================

The CactusConfiguration module contains classes and functions for
manipulating Cactus configurations.  This module provides a Python
interface to the functionality exposed by the Cactus make system only,
and is not intended to be used directly by an end-user.

"""

import os

def get_configurations():
    """Return a list of CactusConfiguration objects representing the
       currently existing Cactus configurations"""
    dirs = os.listdir("configs")
    configs = []
    for d in dirs:
        if os.path.exists("configs/%s/config-data" % d):
            configs.append(d)
    return map(CactusConfiguration, configs)

class CactusConfiguration:
    """Creating a new CactusConfiguration object binds that new object to a
       Cactus configuration called *name*.  This configuration need
       not exist yet in the filesystem.  The CactusConfiguration class
       provides similar semantics to the Cactus make system.
       """

    def __init__(self, name):
        """Create a new CactusConfiguration object and bind it to a (possibly nonexistent)
        Cactus configuration called *name*"""
        self.name = name

    def __exists(self):
        os.path.exists(os.path.join("configs",self.name))

    def __repr__(self):
        return "CactusConfiguration(\"%s\")" % self.name

    def __str__(self):
        return self.name

    def build(self, reconfigure=False, options=None, thornlist=None, debug=None, optimise=None):
        """Build the configuration.  If any of the optional arguments
        *options*, *thornlist*, *debug* or *optimise* are different to
        None, or *reconfigure* is True, the configuration will be
        reconfigured as well as compiled.

        Cactus configurations are built in two stages.  The first
        stage is called *configuration*, during which the
        configuration options are specified and autoconf is run to
        determine the configuration appropriate for the current
        system.  The second stage is called *compilation*, during
        which the CST is run, the source files are compiled, and the
        final executable is produced.  Cactus does not provide
        fine-grained control over these two stages, so neither does
        the *build* method.  If the configuration does not yet exist,
        or reconfiguration is required (see above), both the
        configuration and build stages will be run.  If the
        configuration does exist, and no configuration options are
        specified, the configuration will be recompiled using the
        last-used options."""

        cmd = ["make", "PROMPT=no"];
        if reconfigure or options != None or thornlist != None or debug != None or optimise != None:
            cmd.append(self.name+"-config")
            if options != None:
                cmd.append("options=%s" % options)
            if thornlist != None:
                cmd.append("THORNLIST=%s" % thornlist)
            if debug != None:
                cmd.append("DEBUG=%s" % debug)
            if optimise != None:
                cmd.append("OPTIMISE=%s" % optimise)
        else:
            cmd.append(self.name)

        if os.system(" ".join(cmd)):
            raise(Exception("CactusConfiguration of %s failed" % self.name))

    def clean(self):
        """Delete all object and dependency files in the configuration"""
        if os.system(" ".join(["make", self.name+"-realclean"])):
            raise(Exception("Clean of %s failed" % self.name))

    def delete(self):
        """Delete the configuration"""
        if os.system(" ".join(["make", self.name+"-delete"])):
            raise(Exception("Deletion of %s failed" % self.name))
