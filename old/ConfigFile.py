"""
==================
ConfigFile module
==================

The ConfigFile module contains the ConfigFile class which represents
a configuration file.  Accessing the entries of the object using
dictionary notation is implemented via direct reads and writes to the
file.  The entire config file is parsed on every read and generated on
every write.
"""

from __future__ import print_function
from collections import MutableMapping
from configparser import ConfigParser
import os

class ConfigFile(MutableMapping):
    """Objects of this class are associated with the file *configfile*
       in the filesystem.  The file does not have to exist, and will
       be created when keys are set.  This class behaves mostly like a
       standard Python dict object."""

    def __init__(self, configfile, section=None, defaults={}):
        self.__configfile = configfile
        self.__config = ConfigParser(defaults, interpolation=None)
        self.__section = (os.path.splitext(os.path.basename(configfile))[0]
            if section == None  else section )
        self.__read() # Get options from file
        self.__write() # Write any unspecified defaults to file

    def __read(self):
        self.__config.remove_section(self.__section)
        self.__config.read([self.__configfile])
        if not self.__config.has_section(self.__section):
            self.__config.add_section(self.__section)

    def __write(self):
        print("Writing data to %s" % self.__configfile)
        self.__config.write(open(self.__configfile, 'w'))

    def __delitem__(self, option):
        self.__config.remove_option(self.__section, option)

    def __getitem__(self, option):
        self.__read()
        try:
            return self.__config.get(self.__section, option)
        except(ConfigParser.NoOptionError):
            raise KeyError

    def __iter__(self):
        return iter(self.__config.options(self.__section))

    def __len__(self):
        return len(self.__config.options(self.__section))

    def __setitem__(self, option, value):
        print("Setting %s to %s" %(option,value))
        self.__config.set(self.__section, option, value)
        self.__write()
