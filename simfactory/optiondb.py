"""
===============
optiondb module
===============

The optiondb module contains the OptionDB and OptionDBEntry classes which
represents a database of options and their entries, respectively.

An option database is a collection of sections with a set of key: value
options for each section. Each section has a unique name and each key has a
unique value for each section. A database is defined by a collection of one
or more YAML files. These files are divided into two types:

  Option database (odb): Each file should have a unique 'name' option which
    is used for the name of that section in the database.
  User database (udb): The user database is a heirarchical tree of classes
    (e.g. Application, Machine, Library), sections (e.g. cactus, mathematica,
    datura), and options. Only options of the class whose name is given by
    OptionDB.UDB_SECTION are considered. All other classes are ignored.
    Sections may already appear in the odb or not. There is a special ALL
    section which may be used to set values for sections.

Within each section, the options allowed and their default values are defined
by the schema of the class given by OptionDB.OPTION_CLASS. If an option
definition appears multiple times, higher-priority definitions generally
replace earlier ones. Since a definition may come from a variety of sources,
the order in which sources are prioritised is:

  1. Section specific options in udb_paths with priority given by the order
     in which they appear in the file list.
  2. Definitions in the ALL sections of udb_paths with priority given by the
     order in which they appear in the file list.
  3. Options in odb_paths. Options for a section may only be defined once in
     all files in odb_paths. If an option appears twice in a section, the
     last definition overrides earlier definitions.
  4. Default value of an option from the schema.

If none of these make a definition, then a key will remain undefined.
"""

from collections import Mapping
import re
import simfactory.yaml as yaml
from simfactory.environment import Environment
from simfactory.utils import lazyprop
from simfactory.utils import merge_dicts
from simfactory.utils import ordered_load
from simfactory.utils import ordered_load_all

class OptionDB(Mapping):
    """
    Option database base class. To create a specific type of option database,
    a class should inherit from OptionDB and override the following member
    variables:
      OPTION_CLASS: This should be a class representing a single entry in the
                    database. A simple example would be 'dict'.
      UDB_SECTION:  A string used to denote the section in the udb
                    corresponding to this class of option.
    """

    OPTION_CLASS = None
    UDB_SECTION = None

    def __init__(self, odb_paths, udb_paths):
        assert isinstance(odb_paths, list)
        assert isinstance(udb_paths, list)
        assert isinstance(self.OPTION_CLASS, type)
        assert isinstance(self.UDB_SECTION, str)

        self._odb_paths = odb_paths
        self._udb_paths = udb_paths

    # Lazy loading of the dictionary containing the database. The data
    # is only loaded when the dictionary is first accessed.
    @lazyprop
    def _odb(self): #pylint: disable=method-hidden
        return self._get_db()

    def __getitem__(self, key):
        return self._odb[key]

    def __iter__(self):
        return iter(self._odb)

    def __len__(self):
        return len(self._odb)

    def __repr__(self):
        return self.__class__.__name__ + '("%r")' % self._odb

    def __str__(self):
        return str(list(self._odb))

    def _get_db(self):
        """Reload all options in the database."""
        # Read options from all files listed in in odb_paths.
        odb = {}
        for filename in self._odb_paths:
            try:
                with open(filename) as odb_file:
                    for options in ordered_load_all(odb_file):
                        if 'name' not in options:
                            raise KeyError('Missing option "name" in %s' % filename)

#                        section = options.pop('name')
                        section = options['name']
                        if section in odb:
                            raise KeyError(str(self.OPTION_CLASS) +
                                           ' "%s" already defined' % section)
                        odb[section] = options
            except IOError:
                # Ignore files which can't be read
                pass

        # Read udb options.
        udb = {}
        for filename in self._udb_paths:
            try:
                with open(filename) as udb_file:
                    try:
                        section = ordered_load(udb_file)[self.UDB_SECTION]
                        if section == None:
                            section = {}
                        udb.update(section)
                    except KeyError:
                        # Ignore cases where there are no options
                        pass
            except IOError:
                # Ignore files which can't be read
                pass

        # Apply options in the ALL section of the udb to all
        # existing sections (which will have been read from the odb)
        common_options = udb.pop('ALL', {})
        for section in odb:
            odb[section].update(common_options)

        # Apply options from the udb
        for (section, options) in udb.items():
            if options == None:
                options = {}
            if section not in odb:
                if 'name' not in options:
                    print("Warning: Ignoring user database entry "+section+
                          " because it does not exist in the option database, and the 'name' "
                          "option required for new entries has not been provided")
                else:
                    odb[section] = common_options
                    odb[section].update(options)
            else:
                for optname, optval in options.items():
                    if isinstance(optval, dict) and 'MERGE' in optval:
                        options[optname] = merge_dicts(odb[section][optname], optval)
                        options[optname].pop('MERGE')
                    if isinstance(optval, list) and 'MERGE' in optval:
                        options[optname] = odb[section][optname] + optval
                        options[optname].remove('MERGE')
                odb[section].update(options)

        # Construct objects for each section
        odb_dict = {}
        for (section, options) in odb.items():
            odb_dict[section] = self.OPTION_CLASS(section, options) #pylint: disable=not-callable
        return odb_dict

    def reload(self):
        self._odb = self._get_db()

class OptionDBEntry(Mapping):
    """
    Class for representing a specific section in an option database.

    This class behaves mostly like a standard Python dict object
    except that it only allows keys from a pre-defined list and values
    must be of a pre-defined type. Not all options are required. If
    they are not given and a default value has been specified then the
    default will be returned.

    To create a specific type of option database entry,
    a class should inherit from OptionDBEngry and override the following
    member variables:
      SCHEMA: This should be a dictionary describing the allowed options,
              their types and optional defaults. A basic example would be:
              {'option1': {'type': str},
               'option2': {'type': int, 'default': 0}}
    """

    SCHEMA = None

    def __init__(self, name, defs):
        assert isinstance(self.SCHEMA, dict)
        self.SCHEMA['name'] = {'type': str}
        _check_name(name)
        self.name = name
        self._defs = {}
        self._set_defaults()
        self._update(defs)

    def _set_defaults(self):
        """Set default values for options."""
        for key, entry in self.SCHEMA.items():
            if 'default' in entry:
                value = entry['default']
                value = _check_type(self.name, key, value, entry['type'])
                self._defs[key] = value

    def _update(self, defs):
        """Update values for options."""
        for key, value in defs.items():
            # Only allow known options
            if key not in self.SCHEMA:
                raise KeyError(str(self.__class__.__name__) +
                               ' entry "%s" contains illegal key "%s"' %
                               (self.name, key))

            value = _check_type(self.name, key, value, self.SCHEMA[key]['type'])
            self._defs[key] = value

    def __getitem__(self, key):
        return self._defs[key]

    def __iter__(self):
        return iter(self._defs)

    def __len__(self):
        return len(self._defs)

    def __repr__(self):
        # TODO: add _defs
        return self.__class__.__name__ + '("%r")' % self._defs

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return vars(self) == vars(other) if isinstance(other, type(self)) else False

    def eval(self, key):
        """Return the value of key after expansion of all variables."""
        env = Environment([self._defs])
        return env.eval(key)

def _check_name(name):
    """Check an entry name is valid."""
    if not isinstance(name, str):
        raise TypeError('section name "%s" is not a string' % name)
    if not re.search(r'^[a-zA-Z][a-zA-Z0-9_]*$', name):
        raise ValueError('section name "%s" '
                         'is not a legal section name' % name)

def _check_type(section, key, value, expected_type):
    """Check an option value has the appropriate type"""
    if expected_type not in [bool, int, float, str, list, dict]:
        raise ValueError('Unknown type "%s" for option "%s"' %
                         (expected_type, key))

    if expected_type is float and isinstance(value, int):
        return float(value)

    if not isinstance(value, expected_type):
        raise ValueError('Invalid type "%s" for value "%s" of option "%s" '
                         'in section "%s"; expected "%s"' %
                         (type(value), value, key, section, expected_type))

    return value
