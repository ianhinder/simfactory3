#!/bin/bash
set -e
echo "Test application running"

if [ $# -ne 1 ]; then
    echo "Wrong number ($#) of arguments; expecting only one, the parameter file" >&2
    exit 1
fi

parfile="$1"
cat "${parfile}"
echo "This is a warning" >&2
touch "done"
