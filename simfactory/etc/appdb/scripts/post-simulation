#!/bin/bash

set -e
set -x
set -u

echo "PATH = $PATH"

env|grep SIMFACTORY

export MATHEMATICA_PREPEND_PATH=${SIMFACTORY_BASE_PATH}/../repos

sim="${SIMFACTORY_BASE_PATH}/sim"

export SIMFACTORY_MACHINE

if [ ${SIMFACTORY_TERMINATION_REASON} = "FinalTime" ]; then

    if [ "${SIMFACTORY_DEFINITION_ECCRED-False}" = True ]; then
        previous_sim_name=$(python -c 'import re; import sys; print re.sub(r"_e(\d)_", lambda m : "_e"+str(int(m.group(1))-1)+"_", sys.argv[1])' ${SIMFACTORY_SIMULATION_NAME})

        if [ -r ${SIMFACTORY_SIMULATIONS_PATH}/${previous_sim_name} ]; then
            AnalyseEccentricity ${SIMFACTORY_SIMULATIONS_PATH}/${previous_sim_name} prev-ecc.out </dev/null
            source prev-ecc.out
            previous_Eccentricity=$Eccentricity
        else
            previous_Eccentricity=1.00
        fi
    
        # Eccentricity reduction
        AnalyseEccentricity ${SIMFACTORY_SIMULATIONS_PATH}/${SIMFACTORY_SIMULATION_NAME} ecc.out </dev/null
        source ecc.out

        EccentricityTarget=${SIMFACTORY_DEFINITION_ECCTARGET-0.0005}
        EccentricityMax=${SIMFACTORY_DEFINITION_ECCMAX-0.001}

        echo "Current eccentricity: ${Eccentricity}"
        echo "Previous eccentricity: ${previous_Eccentricity}"
        echo "Eccentricity target: ${EccentricityTarget}"
        echo "Maximum allowed eccentricity: ${EccentricityMax}"

        # Three things we can do:
        # 1. Start a new reduction iteration: action=NewIteration
        # 2. Continue the current iteration as the final run: action=ContinueCurrent
        # 3. Stop because we have not reached an acceptable eccentricity: action=Abort

        action=None
        if awk "BEGIN{exit !(${Eccentricity} < ${EccentricityTarget})}"; then
            action=ContinueCurrent
            echo "Eccentricity ${Eccentricity} is less than the target ${EccentricityTarget}; continuing current simulation and submitting higher resolutions"
        elif awk "BEGIN{exit !(${Eccentricity} < 0.8*${previous_Eccentricity})}"; then
            action=NewIteration
            echo "Eccentricity ${Eccentricity} is greater than the target ${EccentricityTarget} but less than 0.8x the previous eccentricity (${previous_Eccentricity}); starting a new iteration"
        elif awk "BEGIN{exit !(${Eccentricity} < ${EccentricityMax})}"; then
            echo "Eccentricity ${Eccentricity} is greater than the target ${EccentricityTarget} and not less than 0.8x the previous eccentricity (${previous_Eccentricity}), but is less than the maximum eccentricity ${EccentricityMax}; continuing current simulation"
            action=ContinueCurrent
        else
            # echo "Eccentricity is not decreasing, or not decreasing quickly enough; aborting"
            # action=Abort
            echo "Eccentricity is not decreasing, or not decreasing quickly enough, trying a new iteration anyway"
            action=NewIteration
        fi

        case "$action" in
            ContinueCurrent)
                # Continue the current iteration, as the eccentricity is small enough
                $sim activate ${SIMFACTORY_SIMULATION_NAME}
                $sim modify --force ${SIMFACTORY_SIMULATION_NAME} --define ECCRED False
                $sim continue --force ${SIMFACTORY_SIMULATION_NAME}

                current_N=${SIMFACTORY_DEFINITION_N}
                pattern=".*$current_N"

                min_N=${SIMFACTORY_DEFINITION_NMIN-$current_N}
                max_N=${SIMFACTORY_DEFINITION_NMAX-$current_N}

                echo "Submitting resolutions between $min_N and $max_N omitting $current_N"
                if [[ ${SIMFACTORY_SIMULATION_NAME} =~ $pattern ]]; then
                    name_base=${SIMFACTORY_SIMULATION_NAME%_${current_N}}

                    for (( new_N=$min_N; new_N<=$max_N; new_N+=4 )); do
                        if [ $new_N -ne $current_N ]; then
                            new_sim_name="${name_base}_${new_N}"
                            echo "Submitting new simulation $new_sim_name"
                            $sim clone ${SIMFACTORY_SIMULATION_NAME} ${new_sim_name}
                            $sim modify ${new_sim_name} --define N $new_N
                            $sim continue ${new_sim_name}
                        fi
                    done

                else
                    echo "ERROR: Simulation name ${SIMFACTORY_SIMULATION_NAME} is not consistent with N = $current_N" >&2
                    exit 1
                fi
                echo "Finished submitting new simulations"
                ;;

            NewIteration)
                # Start a new eccentricity reduction iteration
                new_sim_name=$(python -c 'import re; import sys; print re.sub(r"_e(\d)_", lambda m : "_e"+str(int(m.group(1))+1)+"_", sys.argv[1])' ${SIMFACTORY_SIMULATION_NAME})
                if [ "$new_sim_name" = "${SIMFACTORY_SIMULATION_NAME}" ]; then
                    echo "Simulation name ${SIMFACTORY_SIMULATION_NAME} does not match pattern *_e*_* required for eccentricity reduction" >&2
                    exit 1
                fi
                $sim clone ${SIMFACTORY_SIMULATION_NAME} ${new_sim_name}
                $sim modify ${new_sim_name} --define D ${NextD} --define PR ${NextPr} # --define ECCRED True
                $sim continue ${new_sim_name}
                ;;

            Abort)
                ;;

            *)
                echo "Internal error" >&2
                exit 1
                ;;
        esac
        
    fi
elif [ ${SIMFACTORY_TERMINATION_REASON} = "SimulationComplete" ]; then
    echo "Exporting to ${SIMFACTORY_SIMULATIONS_PATH}/${SIMFACTORY_SIMULATION_NAME}/exported"
#    env|grep intel
    ExportBBHSimulation --simulation ${SIMFACTORY_SIMULATIONS_PATH}/${SIMFACTORY_SIMULATION_NAME} --exportDir ${SIMFACTORY_SIMULATIONS_PATH}/${SIMFACTORY_SIMULATION_NAME}/exported --email ${SIMFACTORY_EMAIL} </dev/null
    echo "Export complete"

    if [ -r ${SIMFACTORY_BASE_PATH}/../repos/ConvertFormat/scripts/CactusToLIGO/convertToLIGO.py ]; then
        echo "Converting to LIGO format"
        python ${SIMFACTORY_BASE_PATH}/../repos/ConvertFormat/scripts/CactusToLIGO/convertToLIGO.py ${SIMFACTORY_SIMULATION_NAME} ${SIMFACTORY_SIMULATIONS_PATH}/${SIMFACTORY_SIMULATION_NAME}/exported/rhOverM_Asymptotic_GeometricUnits.h5 ${SIMFACTORY_SIMULATIONS_PATH}/${SIMFACTORY_SIMULATION_NAME}/exported

        echo "Conversion to LIGO format complete"
    fi
fi
echo "Post-simulation script complete"
