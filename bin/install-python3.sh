#! /bin/sh

# Download and install Python 3
# 2014-01-19 Erik Schnetter <schnetter@gmail.com>

# Run this script in the directory where Python 3 should be installed.
# Python 3 will then be installed into a subdirectory "python-3.4.4"
# of this directory. For example, to install Python 3 into
# $HOME/python-3.4.4/bin, run this script in your home directory:

#    cd $HOME; ./install-python3.sh

# Why do we use a subdirectory python-3.4.4"? This ensures that we
# don't accidentally overwrite other software, and that uninstalling
# other software won't accidentally remove Python 3 as well.
# Simfactory automatically looks into $HOME/python-3.4.4 when looking
# for a good Python version.



# Remember the install directory
install_dir=`pwd`/python-3.4.4

# Switch to /tmp for downloading and building
cd /tmp
mkdir "$USER"
cd "$USER"

# Download
echo 'Downloading Python 3.4.4...'
wget --no-check-certificate https://www.python.org/ftp/python/3.4.4/Python-3.4.4.tgz

# Unpack
echo 'Unpacking Python 3.4.4...'
rm -rf Python-3.4.4
tar xzf Python-3.4.4.tgz

# Configure, build, install
cd Python-3.4.4
echo 'Configuring Python 3.4.4...'
./configure --prefix="$install_dir"
echo 'Building Python 3.4.4...'
make
echo 'Installing Python 3.4.4...'
make install

# Output Python's version number for testing
echo 'Testing Python 3.4.4...'
echo "Running $install_dir/bin/python3 --version"
"$install_dir/bin/python3" --version

# Delete tarball and build directory
echo 'Cleaning up...'
cd ..
rm -rf Python-3.4.4.tgz Python-3.4.4

echo 'Done.'
