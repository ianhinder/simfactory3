import filecmp
import os
import os.path
import shutil
import tempfile
import unittest

from simfactory.application import Application
from simfactory.build import Build
from simfactory.paths import testspath
import simfactory.build

class TestBuild(unittest.TestCase):

################################################################################

    def setUp(self):
        self.tempdir = tempfile.mkdtemp()
#        print("tempdir = "+self.tempdir)

    def tearDown(self):
        shutil.rmtree(self.tempdir)
        pass # pylint: disable=no-member

    def test_clone_exe(self):
        buildrefdir = os.path.join(self.tempdir, "build"); os.mkdir(buildrefdir)
        exefile = os.path.join(testspath(), "testapp", "testapp.sh")
        app = Application("myapp", {})
        b1 = Build("mybuild", app, None, exefile)
        b2 = simfactory.build.clone(b1, buildrefdir)
        buildreffile = os.path.join(buildrefdir, "testapp.sh")
        self.assertTrue(os.path.exists(buildreffile))
        self.assertTrue(os.path.samefile(b2.exe, buildreffile))
        self.assertTrue(filecmp.cmp(b2.exe, buildreffile, shallow=False))

    def test_construct_build(self):
        name = 'mybuild'

        app1 = Application('myapp1', {})

        exe = os.path.abspath(testspath()+"/testapp/testapp.sh")
        app2 = Application('myapp2', {'executable': exe})

        app3 = Application('myapp2', {'executable': 'non-existant-file'})

        # Check required arguments are handled
        with self.assertRaises(Exception):
            b = Build(None, app1)

        with self.assertRaises(Exception):
            b = Build(name, None)

        with self.assertRaises(Exception):
            b = Build(None, None)

        # Check executable handling
        b = Build('mybuild', app1, exe=exe)
        self.assertEqual(b.exe, exe)

        b = Build('mybuild', app1, exe=None)
        self.assertEqual(b.exe, None)

        b = Build('mybuild', app2, exe=None)
        self.assertEqual(b.exe, exe)

        with self.assertRaises(Exception):
            b = Build(name, app3)

################################################################################


################################################################################


if __name__ == '__main__':
    unittest.main(verbosity=2)
