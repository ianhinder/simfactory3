import datetime
import unittest
import glob
import os
import random
import shutil
import subprocess
import tempfile
import time

import simfactory.paths
import simfactory.mdb
import simfactory.utils

class TestUtils(unittest.TestCase):

    def setUp(self):
        self.tempdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tempdir)

    def test_quote(self):
        def get_randelt(elts):
            return elts[random.randint(0,len(elts)-1)]
        def get_goodchar():
            return get_randelt('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-.')
        def get_badchar():
            return get_randelt(' \n\\!\'"`$')
        def get_randchar():
            return chr(random.randint(1,127))
        def get_char():
            return get_randelt([get_goodchar, get_badchar, get_randchar])()
        for n in range(100):
            # choose a random string length, including a empty string
            slen = random.randint(0,10)
            # choose a random string
            ostr = ''.join(map(lambda i: get_char(), range(slen)))
            # quote it...
            qstr = simfactory.utils.quote(ostr)
            # ...and parse it with a shell
            # Note: We can't parse with shlex; this doesn't handle "\$"
            # correctly
            pbytes = subprocess.check_output(
                ' '.join(['/usr/bin/env', 'python', '-c',
                          "'import sys\nfor x in sys.argv[1]: print ord(x)'",
                          qstr]),
                shell=True, universal_newlines=True)
            pstr = ''.join([chr(int(x)) for x in pbytes.split()])
            self.assertEqual(pstr, ostr)

    def test_convert_to_remote_dir(self):
        self.mdb_yamls = glob.glob(os.path.join(simfactory.paths.testspath(), 'testmdb/mdb*.yaml'))
        self.udb_yaml = os.path.join(simfactory.paths.testspath(), 'testmdb/udb.yaml')
        self.mdb = simfactory.mdb.MachineDB(self.mdb_yamls, [self.udb_yaml])
        lm = self.mdb['test']
        rm = self.mdb['other']
        self.assertEqual(simfactory.utils.convert_to_remote_dir(lm,rm,'/Users/username/proj/src'),
                         '/home/myalias/codes/proj/src')

    def test_working_directory(self):
        with simfactory.utils.working_directory(self.tempdir):
            self.assertTrue(os.path.samefile(os.getcwd(), self.tempdir))

    def test_working_directory_escape(self):
        oldwd = os.getcwd()

        try:
            with simfactory.utils.working_directory(self.tempdir):
                self.assertTrue(os.path.samefile(os.getcwd(), self.tempdir))
                # TODO: use a custom exception
                raise Exception("escaped from working_directory")
        except:
            self.assertTrue(os.path.samefile(os.getcwd(), oldwd))

    def test_parse_walltime(self):
        dt = simfactory.utils.parse_walltime("24:00:00")
        self.assertEqual(dt, datetime.timedelta(hours=24,minutes=0,seconds=0))

        dt = simfactory.utils.parse_walltime("1:2:3")
        self.assertEqual(dt, datetime.timedelta(hours=1,minutes=2,seconds=3))

        with(self.assertRaises(Exception)):
            simfactory.utils.parse_walltime("1:2:3:4")

        with(self.assertRaises(Exception)):
            simfactory.utils.parse_walltime("1:b:3")

        with(self.assertRaises(Exception)):
            simfactory.utils.parse_walltime("1:2")

    def test_wait_for(self):
        reftime = time.time()
        self.assertFalse(simfactory.utils.wait_for(lambda: time.time() > reftime+1, timeout=0.1))
        reftime = time.time()
        self.assertTrue(simfactory.utils.wait_for(lambda: time.time() > reftime+0.1, timeout=1))

    def test_file_string_replace(self):
        test_file = os.path.join(self.tempdir, 'replace.par')
        shutil.copy(os.path.join(simfactory.paths.testspath(),'replace.par'), test_file)
        simfactory.utils.file_string_replace(test_file,
                                             {'@VALUE3@' : '9',
                                              '@WALLTIME_HOURS@' : '24'})
        with open(test_file) as f:
            new_content = f.read()
        with open(os.path.join(simfactory.paths.testspath(), 'replace2.par')) as f:
            ref_content = f.read()
        self.assertEqual(new_content, ref_content)

if __name__ == '__main__':
    unittest.main(verbosity=2)
