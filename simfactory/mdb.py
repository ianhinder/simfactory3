"""
==========
mdb module
==========

The mdb module contains the mdb object which represents a database of machines.

The mdb module contains the mdb object which represents a database of machines.
The mdb object behaves like an OptionDB, where the members are instances of the
Machine class and where only the "Machine" section of the udb is scanned.
"""

import glob
import os
import re
import socket

from simfactory.optiondb import OptionDB
from simfactory.machine import Machine
import simfactory.paths

__all__ = ['mdb']

# Set explicitly by other code

# TODO: since this is a Machine object, it has a lot of state.  Do we
# ever modify Machine objects?  Will this cached version become out of
# date?  Would it be better to store the machine name instead?  Or
# does this always end up being a reference to the same object?

local_machine = None    # pylint: disable=invalid-name


class MachineDB(OptionDB):
    """
    Machine database object.
    """

    OPTION_CLASS = Machine
    UDB_SECTION = "Machine"

    def probe_local_machine(self): # Backward compatibility
        return self.get_local_machine()

    def get_local_machine(self):
        """"
        Return the local machine (i.e. the machine where SimFactory is executing)
        """

        global local_machine # pylint: disable=invalid-name

        if local_machine:
            return local_machine

        name = None
        # environment variable
        try:
            name = os.environ['SIMFACTORY_MACHINE']
        except KeyError:
            pass
        # match hostname patterns
        if not name:
            hostname = socket.gethostname()
            candidates = [m for m in self.values()
                          if re.search(m['hostnamepattern'], hostname)]
            if not candidates:
                raise Exception('Unrecognised local machine "%s"' % hostname)
            elif len(candidates) > 1:
                raise Exception('Ambiguous local machine, candidates are %s' %
                                [[c['name'],c['hostnamepattern']] for c in candidates])
            name = candidates[0].name
        if not name:
            raise Exception('Could not determine local machine')
        try:
            m = mdb[name]
        except KeyError:
            raise KeyError(
                'No machine with name "%s" is available '
                '(use command list-machines to see all available machines)' %
                name)

        # Cache the result of determining the local machine.  It is a
        # package variable and cannot change.
        local_machine = m
        return m

def set_local_machine(machine):
    global local_machine
    local_machine = machine

_MDB_FILES = glob.glob(os.path.join(simfactory.paths.mdbpath(), '*.yaml'))
_UDB_FILES = [os.path.expanduser('~/.simfactory.yaml'),
              os.path.join(simfactory.paths.basepath(), 'simfactory.yaml')
             ]

mdb = MachineDB(_MDB_FILES, _UDB_FILES)
