
################################################################################

"""
=====================
application module
=====================

The application module contains the Application class which provides
an abstraction over applications which can be used with SimFactory.

An application does not correspond to a particular executable or
source tree. Instead, "application" refers to e.g. "Cactus" or
"Mathematica" in the abstract sense. "Application" should probably be
renamed to "ApplicationType" or similar. More brainstorming is needed
to decide this.

"""

################################################################################

import os
import fnmatch

from simfactory.paths import basepath
from simfactory import optiondb

class Application(optiondb.OptionDBEntry):
    """
    The Application class encapsulates an application which can be
    used by SimFactory, for example Cactus, Mathematica, or a generic
    exe/par combination.
    """

    SCHEMA = {
        'checkpoint-pattern': {
            'type': str,
            'description': 'Filename pattern for checkpoint files',
            'example': '*chkpt.it_*'},
        'default-build': {
            'type': str,
            'description': 'Name of the default build',
            'example': 'sim',
            'default': 'build'},
        'executable': {
            'type': str,
            'description': 'Filename of application executable',
            'example': '{app-path}/exe/cactus_{build}'},
        'fingerprint-file': { # Relative to the app-path
            'type': str,
            'description': 'A file which, if preseent, uniquely '
                           'identifies the application type',
            'example': 'src/include/cGH.h'},
        'mdbkeys': {
            'type': str,
            'description': 'The mdb keys this code defines. We '
                           'probably want a pointer to schema '
                           'entries for this application. Machine '
                           'definitions will contain these entries.',
            'example': 'mathscript'},
        'runcmd': {
            'type': str,
            'description': 'Command to use to run the application',
            'example': '{parallel-runprefix} {executable} -L 3 {par}',
            'default': '{serial-runprefix} {executable} {par}'},
        'sync-filter-rules': {
            'type': list,
            'description': 'List of filter rules to use when syncing',
            'example': ['-p .git', '-p .svn'],
            'default': []},
        'termination-reasons': {
            'type': dict,
            'description': 'List of reasons for a job terminating',
            'example': [],
            'default': {}},
        'termination-actions': {
            'type': dict,
            'description': 'List of actions to be taken on job termination',
            'example': [],
            'default': {}},
        'progress-pattern': {
            'type': str,
            'description': 'Pattern to identify simulation progress',
            'example': 'Simulation progress .*%',
            'default': ""},
        'pause-file' : {
            'type': str,
            'description': ('File name of file used to pause a simulation, '+
                            'relative to the segment directory'),
            'example': 'data/termination.txt',
            'default': ""},
        'parfile_in_output_directory' : {
           'type': bool,
            'description': ('Whether the parfile should be copied into the'+
                            'output directory and run from there'),
            'example': False,
            'default': False},
    }

    def probe_path(self):
        """ Return the application directory containing the fingerprint
        file, or None if this cannot be determined
        """

        if 'fingerprint-file' in self._defs:
            fingerprint_file = self._defs['fingerprint-file']
        else:
            return None

        # Look for the application starting from the current directory or
        # from SimFactory's install directory
        startpaths = [os.getcwd(), os.path.dirname(basepath())]
        for startpath in startpaths:
            path = startpath
            # Successively shorten the path to check parent directories too
            paths = []
            while True:
                paths.append(path)
                newpath = os.path.dirname(path)
                if newpath == path:
                    break
                path = newpath
            for path in paths:
                if os.path.exists(os.path.join(path, fingerprint_file)):
                    return path
        return None

    # BW: does this really belong here?

    # ICH: the logic is that many different applications may have a
    # checkpointing mechanism which can be used by linking files from
    # one segment to another.  The details, such as the filename
    # pattern, would be provided by the application definition, but
    # the code for doing the copying is provided here as it might be
    # generally useful.  We will have to decide if we want to have
    # specific python code for each application, or if we can write
    # this once, parameterised by data in the application db.

    def prepare_recovery(self, previous_dirs, this_dir):
        """Arrange that checkpoint files found in the given
        directories will be used when this application is run."""

        if 'checkpoint-pattern' in self._defs:
            for previous_dir in reversed(previous_dirs):

                # Cannot use shutil.copytree as it requires the
                # destination directory to not exist
                found = False
                for dirpath, _, files in os.walk(previous_dir):
                    for previous_file in files:
                        if fnmatch.fnmatch(previous_file,
                                           self._defs['checkpoint-pattern']):
                            found = True
                            previous_path = os.path.join(dirpath, previous_file)
                            new_file = os.path.join(
                                this_dir,
                                os.path.relpath(previous_path,
                                                start=previous_dir))

                            # ICH: Should we be verbose here?
                            # print("Link", previous_path, "->", new_file)
                            if not os.path.exists(new_file):
                                dirname = os.path.dirname(new_file)
                                if not os.path.exists(dirname):
                                    os.makedirs(dirname)
                                os.link(previous_path, new_file)
                            # TODO: fall back to copying if the linking fails
                # Stop after finding a directory with checkpoint files
                if found:
                    return

    def get_checkpoint_files(self, path):
        filenames = []
        if 'checkpoint-pattern' in self._defs:
            for dirpath, _, files in os.walk(path):
                for filename in files:
                    if fnmatch.fnmatch(filename,
                                       self._defs['checkpoint-pattern']):
                        filenames.append(os.path.relpath(os.path.join(dirpath, filename), path))
        return filenames
