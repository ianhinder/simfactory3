"""
==============
machine module
==============

The machine module contains the Machine class which represents a
machine.
"""

from simfactory import optiondb

class Machine(optiondb.OptionDBEntry):
    """
    A class for storing information about a machine.
    """

    # TODO: Store this in a yaml file
    SCHEMA = {

        # Remote access

        'longname': {
            'type': str,
            'description': 'Human-readable machine name',
            'example': 'Queen Bee'},
        'location': {
            'type': str,
            'description': 'Geographical location of the machine',
            'example': 'LONI',
            'default': ''},
        'description': {
            'type': str,
            'description': 'Human-readable description of the machine',
            'example': 'The large LONI Linux cluster',
            'default': ''},
        'webpage': {
            'type': str,
            'description': 'URL with more detailed information about this machine',
            'example': 'http://www.loni.org/systems/system.php?system=QueenBee',
            'default': ''},
        'status': {
            'type': str,
            'pattern': '(personal|experimental|production|storage|trampoline|outdated)',
            'description': """
                Should be one of:
                  personal:     belongs to someone, use only with permission
                  experimental: may not work properly
                  production:   should work out of the box
                  storage:      used only for storing data
                  trampoline:   only used for tunneling
                  outdated:     do not use it any more
                """,
            'example': 'production'},
        'pythoncmd': {
            'type': str,
            'description': 'Location of Python 3 executable on this machine',
            'example': '/usr/local/bin/python3.2'},
        'user': {
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]+',
            'description': 'User name (login id)',
            'example': 'somebody'},
        'email': {
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]+@[a-zA-Z0-9_+-.]+',
            'description': "User's email address",
            'example': 'sombody@somewhere.com'},
        'hostname': {
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]',
            'description':
                "The machine's host name under which it can be accessed from the outside",
            'example': 'queenbee.loni.org'},
        'internalsubmissionhost': { # TODO: probably rename this
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]',
            'description': 'A host reachable from compute nodes on which job submission commands'
                           ' can be run',
            'example': 'qb1'},
        'hostnamepattern': {
            'type': str,
            'description': 'Regular expression that matches the host name as seen on this machine',
            'example': 'qb[0-9](\.loni\.org)?'},
        'iomachine': {
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]+',
            'description': "Machine name of a machine which can access the machine's file "
                           "system faster",
            'example': 'queenbee'},
        'trampoline': {
            'type': str,
            'pattern': '[a-zA-Z0-9_+-.]+',
            'description': 'Machine name of an intermediate machine which needs to be used to '
                           'access this machine (if it is not directly accessible from the '
                           'outside)',
            'example': 'queenbee'},
        'envsetup': {
            'type': str,
            'description': 'Shell command to prepare the local environment before executing '
                           'a shell command on this machine',
            'example': 'export PATH=/usr/local/bin:{$PATH}',
            'default': ''},
        'rsynccmd': {
            'type': str,
            'description': 'Location of rsync executable on this machine',
            'example': '/usr/local/bin/rsync',
            'default': 'rsync'},
        'remotersyncopts': {
            'type': str,
            'description': 'rsync options necessary for transferring to this machine',
            'example': '-c',
            'default': ''},
        'localrsyncopts': {
            'type': str,
            'description': 'additional rsync options necessary for transferring from this machine',
            'example': '-c',
            'default': ''},
        'sshvariant': {
            'type': str,
            'pattern': '^(ssh|gsissh)$',
            'description': 'ssh-like command necessary to access this machine',
            'example': 'gsissh',
            'default': 'ssh'},
        'sshcmd': {
            'type': str,
            'description': 'Location of ssh executable on this machine',
            'example': 'ssh',
            'default': 'ssh'},
        'gsisshcmd': {
            'type': str,
            'description': 'Location of gsissh executable on this machine',
            'example': 'gsissh',
            'default': 'gsissh'},
        'remotesshopts': {
            'type': str,
            'description': 'ssh options necessary for accessing this machine',
            'example': '-t -Y',
            'default': '-t -Y'},
        'localsshopts': {
            'type': str,
            'description': 'Local ssh options that should be added to sshopts when calling ssh '
                           'on this machine',
            'example': '-o UserKnownHostsFile=@SOURCEDIR@/simfactory/etc/ssh_known_hosts',
            'default': ''},
        'mailcmd': {
            'type': str,
            'description': 'Command used to send email',
            'example': 'mail -s "{mailsubject}" {mailrecipient} <<< "{mailcontent}"'},

        # Storing source code and building

        'sourcebasedir': {
            'type': str,
            'description': 'Base path where source trees should be stored (should be a persistent '
                           ' file system, does not need to be visible to compute nodes)',
            'example': '/home/{user}'},
        'disabled-thorns': {
            'type': list,
            'description': 'List of thorns that cannot be built on this machine',
            'example': ['ExternalLibraries/BLAS', 'ExternalLibraries/LAPACK'],
            'default': []},
        'enabled-thorns': {
            'type': list,
            'description': 'List of thorns that should be enabled on this machine',
            'example': ['ExternalLibraries/BLAS', 'ExternalLibraries/LAPACK'],
            'default': []},
        'optionlist': {
            'type': str,
            'description': 'Option list file name',
            'example': 'queenbee.cfg'},
        'thornlist': {
            'type': str,
            'description': 'Thorn list file name',
            'example': 'manifest/einsteintoolkit.th'},
        'parallel-runprefix': {
            # ICH: should we have a default of "mpirun", or is this so unlikely to
            # be the right one that we want it to be explicitly specified?  On
            # laptops etc, maybe it would be convenient to have the default.
            'type': str,
            'description': 'Shell command to execute a distributed program (appended to '
                           'the command)',
            'example': 'env OMP_NUM_THREADS=@NUM_THREADS@ mpirun -np @NUM_PROCS@'},
        'serial-runprefix': {
            'type': str,
            'description': 'Shell command to execute a serial program (appended to the command)',
            'example': 'env OMP_NUM_THREADS={threads}',
            'default': ''},
        'make': {
            'type': str,
            'description': 'GNU compatible make command',
            'example': 'gmake -j 4',
            'default': 'make'},
        'basedir': {
            'type': str,
            'description': 'Base path where simulation results should be stored (should be a '
                           'large, efficient file system visible to both front end and '
                           'compute nodes)',
            'example': '/scratch/@USER@/simulations'},
        'num_sockets': {
            'type': int,
            'description': 'number of sockets (memory units) per node',
            'example': 2,
            'default': 1},
        'num_cores': {
            'type': int,
            'description': 'number of cores per node, also default number of cores for a job',
            'example': 4,
            'default': 1},
        'num_pus': {
            'type': int,
            'description': 'number of PUs (processing units) per core',
            'example': 1,
            'default': 1},
        'max_num_threads': {
            'type': int,
            'description': 'maximum number of threads per process',
            'example': 8,
            'default': 1},
        'default_num_threads': {
            'type': int,
            'description': 'suggested number of threads per process',
            'example': 4,
            'default': 1},
        'max_num_smt': {
            'type': int,
            'description': 'maximum number of SMT threads',
            'example': 1,
            'default': 1},
        'default_num_smt': {
            'type': int,
            'description': 'suggested number of SMT threads',
            'example': 1,
            'default': 1},
        'memory': {
            'type': float,
            'description': 'amount memory per node in GB',
            'example': 8.0},
        'cpufreq': {
            'type': float,
            'description': 'CPU frequency in GHz',
            'example': 2.33},
        'flop_per_cycle': {
            'type': float,
            'description': 'flop per cycle of each core (for all SMT threads combined)',
            'example': 4.0},
        'efficiency': {
            'type': float,
            'description': 'fraction of theoretical peak performance that one typically achieves',
            'example': 0.1,
            'default': 0.1},
        'default_allocation': {
            'type': str,
            'description': 'Allocation id',
            'example': 'loni_numrel08'},
        'default_queue': {
            'type': str,
            'description': 'Queue name',
            'example': 'production'},
        'max_num_nodes': {
            'type': int,
            'description': 'maximum number of nodes that can be used in a single job',
            'example': 256},
        'default_num_nodes': {
            'type': int,
            'description': 'default number of nodes for a job',
            'example': 256,
            'default': 1},
        'min_num_cores': {
            'type': int,
            'description': 'minimum number of cores per node that needs to be requested',
            'example': 8},
        'max_walltime': {
            'type': str,
            'pattern': '^\d+:\d+:\d+$',
            'description': 'Run time limit (HH:MM:SS), also default run time limit',
            'example': '48:00:00',
            'default': '8760:00:00'},
        'max_queue_slots': {
            'type': int,
            'description': 'Maximum allowed number of jobs in queue per user',
            'example': 50},
        'submitcmd': {
            'type': str,
            'description': 'Shell command to submit a job (passed as stdin) to the queuing system',
            'example': 'qsub'},
        'submitpattern': {
            'type': str,
            'description': 'Regular expression to extract the job id from the submit command',
            'example': '^(\d+)'},
        'interactivecmd': {
            'type': str,
            'description': 'Shell command to submit an interactive job to the queuing system',
            'example': 'qsub -I'},
        'statuscmd': {
            'type': str,
            'description': 'Shell command to examine the job status',
            'example': 'qstat @JOB_ID@'},
        'statuspattern': {
            'type': str,
            'description': 'Regular expression to examine the job status',
            'example': '^@JOB_ID@[.]'},
        'queuedpattern': {
            'type': str,
            'description': 'Regular expression to determine whether the job is queued',
            'example': '^@JOB_ID@[.].* Q'},
        'runningpattern': {
            'type': str,
            'description': 'Regular expression to determine whether the job is queued',
            'example': '^@JOB_ID@[.].* R'},
        'stoppedpattern': {
            'type': str,
            'description': 'Regular expression to determine whether the job is stopped',
            'example': '^@JOB_ID@[.].* STP'},
        'heldpattern': {
            'type': str,
            'description': 'Regular expression to determine whether the job is '
                           'queued(aka presubmitted)',
            'example': '^@JOB_ID@[.].* H'},
        'stopcmd': {
            'type': str,
            'description': 'Shell command to stop a job',
            'example': 'qdel @JOB_ID@'},
        'scratchbasedir': {
            'type': str,
            'description': 'A fast directory where temporary files can be stored. This directory '
                           'may not be available on the head node, and files may be deleted when '
                           'a job has finished.',
            'example': '/scratch'},
        'exechostcmd': {
            'type': str,
            'description': 'Command to find root compute node host name',
            'example': 'qstat -f @JOB_ID@'},
        'exechostpattern': {
            'type': str,
            'description': "Regular expression to determine the compute node's host name",
            'example': 'exec_host = (\w+)/'},
        'stdoutcmd': {
            'type': str,
            'description': 'Command to list stdout of a running job (will be executed in the '
                           'restart directory)',
            'example': 'ssh @EXECHOST@ cat /var/spool/torque/spool/@JOB_ID@.qb2.OU'},
        'stderrcmd': {
            'type': str,
            'description': 'Command to list stderr of a running job (will be executed in the '
                           'restart directory)',
            'example': 'ssh @EXECHOST@ cat /var/spool/torque/spool/@JOB_ID@.qb2.ER'},
        'stdout_follow': {
            'type': str,
            'description': 'Command to list and follow stdout of a running job (similar to '
                           '"tail -f") (will be executed in the restart directory)',
            'example': 'ssh @EXECHOST@ tail -n 100 -f /var/spool/torque/spool/@JOB_ID@.qb2.OU '
                       '/var/spool/torque/spool/@JOB_ID@.qb2.ER'}
    }
