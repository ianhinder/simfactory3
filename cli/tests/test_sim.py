import os, sys
import unittest
import tempfile
import subprocess

from cli import sim
from simfactory import mdb
import shutil

from simfactory.utils import stdout_redirect
from simfactory.utils import working_directory

import simfactory.paths

class _FakeArgs:
    def set(self, attr, value):
        setattr(self, attr, value)

class TestsSim(unittest.TestCase):
    def setUp(self):
        self.args = _FakeArgs()
        self.args.set('quiet', False)
        self.args.set('application_name', None) # Pretend that no --app was given
        self.args.set('build', None) # Pretend that no --build was given
        self.args.set('local_machinename', None) # Pretend that no --machine was given
        self.tempdir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.tempdir)
        pass

    # MDB

    def test_list_machines(self):
        try:
            old_stdout = sys.stdout
            with open('/dev/null', 'w') as sys.stdout:
                sim.cmd_list_machines(self.args)
        finally:
            sys.stdout = old_stdout
        self.assertTrue(True)

    def test_show_machine(self):
        machinename = list(mdb.mdb)[0]
        self.args.set('machinename', machinename)
        try:
            old_stdout = sys.stdout
            with open('/dev/null', 'w') as sys.stdout:
                sim.cmd_show_machine(self.args)
        finally:
            sys.stdout = old_stdout
        self.assertTrue(True)

    def test_local_machine(self):
        try:
            old_stdout = sys.stdout
            with open('/dev/null', 'w') as sys.stdout:
                sim.cmd_local_machine(self.args)
        finally:
            sys.stdout = old_stdout
        self.assertTrue(True)

    # AppDB

    def test_application_path(self):

        cactus_path = os.path.abspath(os.path.join(self.tempdir, "Cactus"))
        os.mkdir(cactus_path)

        with working_directory(cactus_path):
            # getcwd returns a path with links resolved, but os.path.abspath does not
            cactus_path = os.getcwd()

        os.makedirs(os.path.join(cactus_path, "src/include"))
        open(os.path.join(cactus_path, "src/include/cctk_Version.h"), 'a').close()
        subdir = os.path.join(cactus_path, "subdir")
        os.mkdir(subdir)

        with working_directory(subdir):
            with open('app.txt', 'w') as out:
                with stdout_redirect(out):
                    sim.cmd_application_path(self.args)

            with open('app.txt', "r") as out:
                self.assertEqual(out.readlines(), ["Application path: "+cactus_path+"\n"])

    def test_status(self): #pylint: disable=no-self-use
        # TODO: add a command line option to use a different basedir,
        # then construct a temporary basedir for the tests with some
        # standard simulations, so we can test that the status output
        # is correct.  Currently we just make sure that the command
        # runs without raising an exception.
        simscript = os.path.join(simfactory.paths.basepath(), "sim")
        # TODO: IH: check that nothing is produced on stderr;
        # currently on my laptop I get "qstat: command not found"
        # because status doesn't know that some simulations may have
        # been synced from another machine, and tries to use the
        # foreign machine queue commands.
        subprocess.check_output([simscript, "status"])

if __name__ == '__main__':
    unittest.main(verbosity=2)
