
from tkinter import *
from tkinter import ttk

import simfactory.simulation

from simfactory.mdb import *

class Application(Frame):

    def cmd_refresh(self):
        self.create_tree()

    def create_tree(self):

        if not self.simulations:
            self.simulations = ttk.Treeview(self, columns=['State', 'Status', 'Cores','Walltime'])
        else:
            for item in self.simulations.get_children():
                print("item=",item)
                self.simulations.delete(item)

        sims = simfactory.simulation.get_simulations(self.machine)

        self.simulations.heading(0, text='State')
        self.simulations.heading(1, text='Status')
        self.simulations.heading(2, text='Cores')
        self.simulations.heading(3, text='Walltime')

        for sim in sims:
            print(sim.name)
            cores = sim.paralleltopology.cores
            walltime = str(sim.walltime) if hasattr(sim, 'walltime') and sim.walltime  else ''
            state = ({True:'Active', False:'Inactive'}[sim.active]) if hasattr(sim, 'active') else ''

            status = {'P': 'Paused', 'Q': 'Queued', 'R': 'Running'}[sim.status()]

            self.simulations.insert('', 'end', text=sim.name, values=[state, status, cores, walltime,state])
            print(vars(sim))
            print()
        self.simulations.pack(expand=1,fill=BOTH)


    def createWidgets(self):

        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack({'side':'top', 'fill':'x'}) # , expand=1

        self.QUIT = Button(frame)
        self.QUIT["text"] = "Quit"
        self.QUIT["fg"] = "red"
        self.QUIT["command"] = self.quit

        self.QUIT.pack({"side": "left"})

        self.refresh = Button(frame)
        self.refresh["text"] = "Refresh",
        self.refresh["command"] = self.cmd_refresh

        self.refresh.pack({"side": "left"})

        self.simulations = None
        self.cmd_refresh()


    def __init__(self, machine, master=None):
        self.machine = machine
        Frame.__init__(self, master)
        self.master.title("Simulation Factory [{}]".format(machine.name))
        self.pack(expand=1,fill=BOTH)

        menubar = Menu(master)
        master.config(menu=menubar)
        sim_menu = Menu(menubar)
        sim_menu.add_command(label="Refresh", command=self.cmd_refresh)
        menubar.add_cascade(label="Simulation", menu=sim_menu)

        self.createWidgets()

machine = mdb.probe_local_machine()

#print(simfactory.simulation.get_simulations(machine))

root = Tk()
# try:
#     root.wm_iconbitmap('snwman.ico')
# except TclError:
#     print('No ico file found')
root.title("Simulation Factory")

# root.mainloop()
app = Application(machine, master=root)
app.mainloop()
root.destroy()
