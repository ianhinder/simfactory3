import unittest
import glob, os

import simfactory.paths
from simfactory import machine
from simfactory import mdb

class TestMachineDB(unittest.TestCase):

    def setUp(self):
        self.mdb_yamls = glob.glob(os.path.join(simfactory.paths.testspath(), 'testmdb/mdb*.yaml'))
        self.udb_yaml = os.path.join(simfactory.paths.testspath(), 'testmdb/udb.yaml')

        self.assertTrue(len(self.mdb_yamls) > 0)
        self.assertTrue(os.path.exists(self.udb_yaml))

        self.mdb = mdb.MachineDB(self.mdb_yamls, [self.udb_yaml])

    def test_list_machines(self):
        # TODO: this seems a strange way to test the keys
        self.assertEqual(sorted(list(self.mdb)), sorted(['test', 'other']))

    def test_have_machine(self):
        self.assertTrue('test' in self.mdb)
        self.assertFalse('test2' in self.mdb)

    def test_get_machine(self):
        m = self.mdb['test']
        self.maxDiff = None
        self.assertEqual(sorted(list(m._defs.items())), sorted(list(machine.Machine('test',
            {'longname': 'Test Machine',
             'location': 'Location Line 1\nLocation Line 2\n',
             'sourcebasedir': '/Users/{user}',
             'user': 'username',
             'name': 'test',
             'email': 'email@address.com'})._defs.items())))

    def test_invalid_machine(self):
        """Check invalid machines are caught"""
        with self.assertRaises(KeyError):
            bad_mdb = mdb.MachineDB([os.path.join(simfactory.paths.testspath(),
                                                   'testmdb/badmdb.yaml')], [])
            bad_mdb.reload()

    def test_mdb(self):
        """Check all machines in the database are valid"""
        bad_machines = []
        for m in mdb.mdb:
            try:
                mdb.mdb[m]
            except (KeyError, ValueError) as e:
                bad_machines += [(m, e.args[0])]
        self.assertListEqual([], bad_machines)

if __name__ == '__main__':
    unittest.main(verbosity=2)
