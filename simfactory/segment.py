
################################################################################

"""
=====================
segment module
=====================

The segment module contains the Segment class which represents a
single segment of a multi-segment simulation.
"""

################################################################################

import os
import pickle
import os.path
import shutil
import stat
import re
import logging
import datetime
import simfactory.yaml as yaml

from simfactory.queue import Queue
from simfactory.environment import Environment
from simfactory.paths import basepath
from simfactory.utils import wait_for

import simfactory.build
import simfactory.utils
import simfactory.appdb
import simfactory.mdb
import collections

################################################################################

def load(segmentdir):
    """Load a simulation segment by path into a new Segment object
    which is returned"""
    # TODO: Think about whether various databases should be refreshed.
    # e.g. the build refers to an application, which may have gained
    # new keys in newer versions of simfactory.  Because we are using
    # pickling, the old build and the old application database will be
    # retrieved here and this may not work with newer versions of
    # simfactory.
    with open(os.path.join(segmentdir, "properties.py"), "rb") as picklefile:
        seg = pickle.load(picklefile)
        seg.setup_logger()
        return seg

################################################################################

class Segment:
    """
    A Segment object represents a segment of a simulation.  It can be
    submitted to a queuing system, or run interactively.  The
    running/not-running state can be queried.
    """

    # Create a new segment
    def __init__(self, name, parentdir, build, par, machine, simname, segmentid,
                 parallel=False, paralleltopology=None, previous_segments=[],
                 walltime=None, postrunscript=None, definitions=[]):
        """Create a new Segment object and store its representation
        on disk in a new directory."""

        self.name = name
        self.dir = os.path.join(parentdir, self.name)

        os.mkdir(self.dir) # Deliberately fails if it exists

        self.logger_name = simname+"."+self.name # TODO: strip '.'s

        # TODO: These should be copied into the simulation directory
        self.par = os.path.abspath(par)
        self.machine = machine
        self.parallel = parallel

        self.metadatadir = os.path.join(self.dir, 'SIMFACTORY')
        os.mkdir(self.metadatadir)
        self.builddir = os.path.join(self.metadatadir, 'build')
        os.mkdir(self.builddir)
        self.build = simfactory.build.clone(build, self.builddir,
                                            parent_logger_name=self.logger_name)
        self.app = self.build.app
        self.pardir = os.path.join(self.metadatadir, 'par')
        os.mkdir(self.pardir)
        shutil.copy(par, self.pardir)
        self.par = os.path.abspath(
            os.path.join(self.pardir, os.path.basename(par)))

        self.paralleltopology = paralleltopology
        self.simname = simname
        self.segmentid = segmentid
        self.outfile = self.simname+".out"
        self.errfile = self.simname+".err"
        self.jobid = None
        self.runpid = None
        self.previous_segments = list(previous_segments)
        self.walltime = walltime
        self.postrunscript = postrunscript
        # TODO: probably want to know the app directly, not rely on it
        # being in a build
        self.definitions = definitions
        self.paused = False
        self.setup_logger()

        self.__save()

################################################################################
# Public methods
################################################################################

    def submit(self):
        """Submit this segment to the queuing system"""

        self.logger().info("Submitting "+self.name)

        pythoncmd = (
            self.machine['pythoncmd'] if 'pythoncmd' in self.machine else
            os.path.abspath(os.path.join(basepath(), "runpython")))

        # TODO: escape sfbase, python and segmentdir. Note
        # double-escaping is complicated because we have to escape for
        # both bash and python.
        script = """#!/bin/bash
PYTHONPATH={sfbase}:$PYTHONPATH {python} -c '
from simfactory import segment
import sys

s = segment.load("{segmentdir}")
sys.exit(s.run())'
""".format(segmentdir=self.dir, python=pythoncmd, sfbase=basepath())

        if self.postrunscript:
            script = script + self._post_run_script_command()
            self._write_post_run_script()

        self.logger().debug("Script file:\n"+script)

        self.jobid = Queue(machine=self.machine).submit(
            script,
            rundir=self.dir,
            paralleltopology=self.paralleltopology,
            out=self.outfile,
            err=self.errfile,
            shortname=self._short_name(),
            req_walltime=self.walltime)
        self.__save()

        # Ensure that the job has appeared in the queue or has run so
        # that callers can rely on the status command.
        if not wait_for(lambda: self.status() != 'U', timeout=20):
            raise Exception(
                "Job did not appear in the queue within 20s of submission")

    def run(self, stdout=None, stderr=None):
        """Run a simulation interactively"""

        self.logger().info("Running segment %s", self.name)

        self._mark_as_started_running()

        self.build.app.prepare_recovery(
            [segment.dir for segment in self.previous_segments], self.dir)

        # TODO: how to specify unlimited walltime?
        # TODO: this should use the machine's maximum walltime, not 10000000
        walltime_hours = str(self.walltime.total_seconds()/3600) if self.walltime else "24"

        parfile_replacements = {"@WALLTIME_HOURS@": walltime_hours,
                                "@SIMULATION_NAME@": self.simname}

        for (key, val) in self.definitions:
            parfile_replacements["@"+key+"@"] = val

        env = Environment([self.machine, self.paralleltopology.env(),
                           {"par": self.par,
                            "parfile-replacements": parfile_replacements}])

        self.logger().debug("Segment run environment: %s", repr(env))

        self.runpid = os.getpid()
        self.__save()

        return self.build.run(env, stdout=stdout, stderr=stderr,
                              cwd=self.dir)

    def stop(self):
        """Stop a simulation in the queueing system"""

        if self.jobid:
            Queue(machine=self.machine).stop(self.jobid)
            self.__save()
        elif self.runpid:
            raise Exception(
                "Cannot currently stop a simulation segment which is running \
interactively")
        else:
            raise Exception(
                "Cannot stop a simulation segment which has never been started")

    def pause(self):
        """Pause a segment cleanly so it can be restarted from the same point"""

        if self.status() == 'R':

            if self.app['pause-file']:
                # TODO: maybe use a standard "simulation environment" which
                # contains properties of the simulation
                env = Environment([self.machine, self.paralleltopology.env(),
                                   {"simname": self.simname}])
                pause_file = os.path.join(self.dir,
                                          env.eval_str(self.app['pause-file']))

                if os.path.exists(pause_file):
                    # Pausing is supported
                    simfactory.utils.write_string_to_file(pause_file, "1")
                    self.paused = True
        elif self.status() == 'Q':
            self.stop()
            self.paused = True

        # If there is no pause file defined, or it doesn't exist, do
        # nothing. The simulation class will mark the simulation as
        # paused, and it will not be resubmitted.  This is the safest
        # implementation of 'pause' for a simulation which does not
        # support termination files.

        # TODO: think about displaying a warning here that the
        # simulation doesn't support pausing, and will run until the
        # end of its walltime, and that it should instead be aborted
        # if the user wants to end it immediately.

    def status(self):
        """Get the status of a simulation segment.  The possible
        status values are Created, Queued, Running, Finished and
        Unknown, and they are indicated by the initial letter."""

        if self.jobid:
            # The segment has been submitted

            # The segment might not have been run on the local machine
            # (it may have been copied to the local machine from
            # another machine), in which case we don't have access to
            # the correct queueing system.  TODO: consider using a
            # remote command here
            if self.machine.name != simfactory.mdb.mdb.get_local_machine().name:
                return 'U'

            jobstatus = Queue(machine=self.machine).status(self.jobid)
            if jobstatus == 'U' and self._did_start_running():
                segstatus = 'F' # TODO: Store this in the segment,
                                # since once finished, it cannot
                                # change
            else:
                segstatus = {'R': 'R', 'Q': 'Q', 'H' : 'H',
                             # Job has not yet appeared in the queue, has been
                             # deleted without going through simfactory, or has
                             # failed to start before running for an unknown
                             # reason.
                             'U': 'U'}[jobstatus]
                if segstatus == 'U' and hasattr(self,'paused') and self.paused:
                    segstatus = 'P'
            return segstatus
        elif self.runpid:
            # TODO: update an attribute in the segment when the run
            # has finished, ensuring that exceptions are caught. This
            # logic here is temporary, and will only work when run on
            # the same host.
            if simfactory.utils.is_child_process_running(self.runpid):
                return 'R'
            else:
                return 'F'
        else:
            # Segment has neither been submitted nor run, only created
            return 'C'

    def termination_reason(self):
        # print("Checking termination reasons")
        # print("app =", self.build.app.items())

        # TODO: try to maintain the order from the YAML file.  See
        # http://stackoverflow.com/questions/5121931/in-python-how-can-you-load-yaml-mappings-as-ordereddicts

        out_path = os.path.join(self.dir, self.outfile)
        err_path = os.path.join(self.dir, self.errfile)
        out_file = simfactory.utils.string_of_file(out_path) if os.path.exists(out_path) else None
        err_file = simfactory.utils.string_of_file(err_path) if os.path.exists(err_path) else None

        term_reasons_unordered = self.build.app['termination-reasons']
        term_reasons = collections.OrderedDict(sorted(term_reasons_unordered.items(), key=lambda t: t[0]))

        for (reason, patterns) in term_reasons_unordered.items():
            # print("Checking termination reason ", r)
            for pattern in patterns:
                if out_file and re.search(pattern, out_file) or err_file and re.search(pattern, err_file):
                    return reason
        # print("No match found")
        return "Unknown"

    def progress_list(self):
        if 'progress-pattern' not in self.build.app:
            return ""
        pattern = self.build.app['progress-pattern']
        if pattern == "":
            return []
        out_path = os.path.join(self.dir, self.outfile)
        if os.path.exists(out_path):
            out_string = simfactory.utils.string_of_file(out_path)
            matches = re.findall(pattern, out_string)
            return matches
        else:
            return []

    def progress(self):
        matches = self.progress_list()
        if len(matches) == 0:
            return ""
        else:
            return matches[-1]

    def made_progress(self):
        plist = self.progress_list()
        print("plist = ", plist)
        if len(plist) == 0:
            return None
        elif plist[0][-1] == plist[-1][-1]:
            return False
        else:
            return True

    def termination_action(self):
        actions = self.build.app['termination-actions']
        reason = self.termination_reason()
        if reason in actions:
            return actions[reason]
        else:
            return None

    def refresh(self):
        """Refresh application database"""
        # TODO: refactor this with simulation class
        app_name = self.build.app['name'] if 'name' in self.build.app else 'cactus'
        # old_app = self.build.app
        # new_app = simfactory.appdb.appdb[app_name]
        # diff = simfactory.utils.diff_structs(old_app,new_app)
        # if len(diff) != 0:
        #     print("WARNING: refreshing application "+app_name+
        #" from SimFactory application database in "+self.name)
        #     print("Changes:")
        #     print(diff)
        self.build.app = simfactory.appdb.appdb[app_name]

        machine_name = self.machine['name'] if 'name' in self.machine else 'datura'
        self.machine = simfactory.mdb.mdb[machine_name]

        self.__save()
        
    def last_output_time(self):
        out_path = os.path.join(self.dir, self.outfile)
        if os.path.exists(out_path):
            return simfactory.utils.file_modification_time(out_path)
        else:
            return None

################################################################################
# Private methods
################################################################################

    def __save(self):
        """Save the current state of this segment to disk."""
        with open(os.path.join(self.dir, "properties.py"), "wb") as picklefile:
            pickle.dump(self, picklefile)

        with open(os.path.join(self.metadatadir, "properties.yaml"), "w") as yamlfile:
            vs = { key:val for (key,val) in self.__dict__.items()
                   if type(val) in [str,int,float,bool,type(None),datetime.timedelta]}
            yaml.dump(vs, yamlfile,default_flow_style=False)
            yaml.dump({'definitions' : {key:val for [key,val] in self.definitions}}, yamlfile,default_flow_style=False)

    def _short_name(self):
        """Return a short name for this segment.  Useful for giving a
        job name in a queuing system."""
        return "{simname}-{restartid}".format(simname=self.simname, restartid=self.segmentid)

    def _mark_as_started_running(self):
        """Mark the simulation as having started running.  This allows
        the status method to tell the difference between a simulation
        which has not yet appeared in the queue and one which has
        finished."""
        open(os.path.join(self.metadatadir, "has_run"), "wb").close()

    def _did_start_running(self):
        """Determine if the segment has ever started running."""
        return os.path.exists(os.path.join(self.metadatadir, "has_run"))

    def _write_post_run_script(self):
        # TODO: this is duplicated from queue.py
        prspath = os.path.abspath(os.path.join(self.metadatadir, "postrunscript.sh"))
        with open(prspath, "w") as scriptfile:
            scriptfile.write(self.postrunscript)
        os.chmod(prspath, os.stat(prspath).st_mode | stat.S_IEXEC)

    def _post_run_script_command(self):
        # Run postrunscript from the head node, as qsub is not
        # available on the compute nodes typically.  TODO: we
        # might need a different ssh hostname from inside the
        # cluster, and on some clusters it might not be possible
        # to ssh from a compute node to the head node
        # TODO: refactor with previous function
        prspath = os.path.abspath(os.path.join(self.metadatadir, "postrunscript.sh"))
        env = Environment([self.machine, {'scriptpath':prspath}])

        # TODO: unify the ssh invocation with that used for remote
        # commands and syncing, if possible

        if 'internalsubmissionhost' in self.machine:
            return env.eval_str("""
{sshcmd} {user}@{internalsubmissionhost} {scriptpath}
""")
        else:
            return env.eval_str("""
{scriptpath}
""")

    def logger(self):
        return logging.getLogger(self.logger_name)

    def setup_logger(self):
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                      datefmt='%m-%d %H:%M:%S')
        handler = logging.FileHandler(filename=os.path.join(self.metadatadir, 'simfactory.log'),
                                      mode='a')
        handler.setFormatter(formatter)
        handler.setLevel(logging.DEBUG)
        self.logger().addHandler(handler)
        self.logger().setLevel(logging.DEBUG)

    def get_error_file(self):
        return os.path.join(self.dir, self.errfile)

    def get_output_file(self):
        return os.path.join(self.dir, self.outfile)
