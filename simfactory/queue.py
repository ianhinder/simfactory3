
################################################################################

"""
=====================
queue module
=====================

The queue module provides a high-level interface to queuing systems.
It contains the Queue class, and a simple background-process system
for testing.
"""

################################################################################

import datetime
import os
import sys
import re
import subprocess
import stat
import textwrap
import time
import logging

import simfactory.utils
from simfactory.environment import Environment
import simfactory.paths
from simfactory.utils import UserException

################################################################################

class Queue:
    """A job submission queue"""
    def __init__(self, machine):
        self.machine = machine

        if 'submitcmd' not in machine:
            raise Exception(textwrap.dedent("""Cannot instantiate a
                 Queue object on a machine with no submitcmd defined.
                 A simple batch system is available in
                 simfactory3/sbs/sbs.py, and an example of the
                 required machine definitions is in (TODO:)
                 iansmacbookpro.ini"""))

        self.submitcmd = machine['submitcmd']
        self.submitpattern = machine['submitpattern']
        self.stopcmd = machine['stopcmd']
        self.statuscmd = machine['statuscmd']
        self.statuspattern = machine['statuspattern']
        self.queuedpattern = machine['queuedpattern']
        self.runningpattern = machine['runningpattern']
        self.stoppedpattern = machine['stoppedpattern'] if 'stoppedpattern' in machine else None
        self.heldpattern = machine['heldpattern']

    def submit(self, script, rundir=".", out="job.out", err="job.err",
               paralleltopology=None, shortname=None, req_walltime=None):
        """Submit a command to a queuing system.  Return a string job
        id to refer to the job.  out and err are basenames for the
        output and error files."""
        # Return object might be a Job object later

        scriptpath = os.path.abspath(os.path.join(rundir, "runscript.sh"))
        with open(scriptpath, "w") as scriptfile:
            scriptfile.write(script)
        os.chmod(scriptpath, os.stat(scriptpath).st_mode | stat.S_IEXEC)

        walltime = (
            req_walltime if req_walltime
            else simfactory.utils.parse_walltime(
                self.machine['max_walltime']))

        env = Environment(
            [self.machine, paralleltopology.env(),
             {'stdout': os.path.join(rundir, out),
              'stderr': os.path.join(rundir, err),
              'scriptfile': scriptpath,
              'simfactorydir': simfactory.paths.basepath(),
              'shortsimname': shortname,
              'walltime': _timedelta_to_str(walltime),
              'walltime_minutes': str(_timedelta_to_minutes(walltime))}])

        fullsubmitcmd = env.eval_str(self.submitcmd)

        logging.info("Submit command: "+fullsubmitcmd)


        try:
            # TODO: capture stderr so it can be reported in the
            # exception.  If not using a CLI, stderr will otherwise be
            # lost.
            output = subprocess.check_output(
                fullsubmitcmd, shell=True, cwd=rundir).decode("utf-8")
        except subprocess.CalledProcessError as cpe:
            raise UserException("Error submitting job: "+cpe.output.decode("utf-8"))

        match = re.search(self.submitpattern, output)
        if not match:
            raise Exception(
                "Cannot determine job id from submission output: "+output)
        job_id = match.group(1)

        logging.info("Job id: "+job_id)
        return job_id

    def stop(self, job_id, wait=False):
        "Stop a running job"
        fullstopcmd = Environment(
            [{'job_id': job_id,
              'simfactorydir': simfactory.paths.basepath()}]
            ).eval_str(self.stopcmd)
        subprocess.check_output(fullstopcmd, shell=True).decode("utf-8")
        if wait:
            while self.status(job_id) != 'U':
                time.sleep(1)

    def status(self, job_id):
        """Return the job status as a string. R means it is running, U
        means it is not found, so is probably finished, and Q means it
        is queued."""
        # TODO: can probably use the machine's env
        env = Environment([{'job_id': job_id, 'user': self.machine['user'],
                            'simfactorydir': simfactory.paths.basepath()}])
        fullstatuscmd = env.eval_str(self.statuscmd)

        try:
            output = subprocess.check_output(
                fullstatuscmd, shell=True).decode("utf-8")
        except subprocess.CalledProcessError:
            output = ""

        if not re.search(env.eval_str(self.statuspattern), output):
            return 'U'
        if re.search(env.eval_str(self.runningpattern), output):
            return 'R'
        if re.search(env.eval_str(self.heldpattern), output):
            return 'H'
        if re.search(env.eval_str(self.queuedpattern), output):
            return 'Q'
        if self.stoppedpattern and re.search(env.eval_str(self.stoppedpattern), output):
            return 'U'

        # TODO: Support other states; e.g. "exiting"
        raise Exception(
            "Cannot determine job status from status output: "+output)

################################################################################
# Private functions
################################################################################

# TODO: maybe have a WallTime object

# Cannot use the str representation of a timedelta because it has a days prefix
def _timedelta_to_hms(delta):
    """Convert a timedelta into an (h,m,s) integer tuple"""
    hour = datetime.timedelta(hours=1)
    minute = datetime.timedelta(minutes=1)

    hours, rest = divmod(delta, hour)
    minutes, rest = divmod(rest, minute)
    seconds = rest.seconds

    return (hours, minutes, seconds)

# TODO: pad with leading zeros if necessary
def _timedelta_to_str(delta):
    """Convert a timedelta into a string of the form h:m:s."""
    return "{}:{}:{}".format(*_timedelta_to_hms(delta))

def _timedelta_to_minutes(delta):
    """Convert a timedelta into minutes."""
    (hours, minutes, seconds) = _timedelta_to_hms(delta)
    return 60*hours+minutes+int(round(seconds/60))
