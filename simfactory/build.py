
################################################################################

"""
=====================
build module
=====================

The build module contains the Build class which provides
an abstraction over builds of applications which can be used with SimFactory.

"""

################################################################################

import os
import sys
import subprocess
import shutil
import copy
import re
import stat
import logging
import simfactory.utils

from simfactory.environment import Environment

from simfactory.utils import UserException

################################################################################

def clone(origbuild, storedir, parent_logger_name=''):
    """Create a new Build object based on an existing one, and copy
    everything needed for it into storedir."""

    # TODO: save disk space and time by hard linking to a cache
    # directory or to previous segments

    shutil.copy(origbuild.exe, storedir)
    newbuild = copy.copy(origbuild)
    newbuild.exe = os.path.join(storedir, os.path.basename(origbuild.exe))
    newbuild.logger_name = simfactory.utils.join_logger_names(parent_logger_name, newbuild.name)
    return newbuild

################################################################################

# TODO: We have similar concepts of an environment for both machines and apps
#       so they should probably be refactored into a common implementation.
class Build:
    """The Build class encapsulates a build of an application which can be
    used by SimFactory.  It can be used to run this build."""

    def __init__(self, name, app, app_path=None, exe=None, parent_logger_name=''):
        """Construct a new build object for the application with the
        given name"""

        if not name:
            raise Exception("Build cannot be created without a name")
        if not app:
            raise Exception("Build cannot be created without an application")
        # TODO: think about whether a build needs an app path
        # if not app_path:
        #     raise Exception("Build cannot be created without an app path")

        self.logger_name = simfactory.utils.join_logger_names(parent_logger_name, name)

        self.name = name
        self.app = app
        self.app_path = app_path

        # Allow a different executable to be specified than the one in
        # the appdb, if any
        if exe:
            self.exe = os.path.abspath(exe)
        elif 'executable' in self.app:
            env = Environment([self.app,
                               {'build': self.name, 'app-path': self.app_path}])
            self.exe = os.path.abspath(env.eval('executable'))
        else:
            self.exe = None # Some applications might not need an executable

        # TODO: store the exe filename relative to the
        # simulation. This way, the simulation is
        # relocatable. Probably need to pass in a base path for this.

        # TODO: This is a good indication that the app requires an
        # executable, but we should also have an explicit flag for
        # this in the app definition
        if 'executable' in self.app:
            if not os.path.exists(self.exe):
                raise UserException("Executable "+self.exe+" required but not found")


    def run(self, replacements, stdout=None, stderr=None, cwd=None):
        """Run the build, using the replacements in the given dict"""
        # TODO: Check that self.name, self.app_path and self.exe are defined
        env = Environment([replacements, self.app,
                           {'build': self.name, 'app-path': self.app_path,
                            'executable': self.exe}])
        self.logger().info("Running build %s", self.name)
        self.logger().debug("Build run environment: " + repr(env))

        real_cwd = cwd if cwd else os.getcwd()

        if 'par' in env and 'parfile-replacements' in env:
            self._replace_parameter_file(env['par'], env['parfile-replacements'])

        if 'par' in env:
            # TODO: pass stdout/stderr in env, or redirect it at a
            # higher level
            env = self._process_parfile_script(env, stdout, stderr, cwd)

            if self.app['parfile_in_output_directory']:
                print("real_cwd = "+real_cwd)
                par_base = os.path.basename(env['par'])
                new_parfile = shutil.copyfile(env['par'], os.path.join(real_cwd,par_base))
                env = Environment([env, {'par': par_base}])

        # At the moment, we need a shell because we are including
        # environment variable settings in the parallel-runcmd.  These
        # should be handled in some other way, probably in an envsetup
        # variable.

        #        cmd = shlex.split(env['runcmd'])
        cmd = env.eval('envsetup') + "\n" + env.eval('runcmd')

        # Flush any debugging output
        sys.stdout.flush()
        sys.stderr.flush()

        self.logger().info("Running command: %s", cmd)

        # We need to use bash here because the envsetup likely is
        # going to call module or source, which assumes a bash shell
        FNULL = open(os.devnull, 'r')
        retcode = subprocess.call(
            ["/bin/bash", "-c", cmd], stdin=FNULL, stdout=stdout, stderr=stderr, cwd=cwd, shell=False)

        sys.stdout.flush()
        sys.stderr.flush()
        return retcode

    def __eq__(self, other):
        return vars(self) == vars(other)

    def _replace_parameter_file(self, parfile, replacements):
        self.logger().debug("Parameter file replacements: "+repr(replacements))
        simfactory.utils.file_string_replace(parfile, replacements)

    def _process_parfile_script(self, env, stdout, stderr, cwd):
        # TODO: The details of script execution, e.g. filename
        # extensions and how the output is returned, should be
        # specified in the application, not hard-coded here
        orig_parfile = env['par']
        par_match = re.match('(.*)\.rpar$', orig_parfile)
        if par_match:
            self.logger().info("Executing parameter file script "+orig_parfile)
            os.chmod(orig_parfile, os.stat(orig_parfile).st_mode | stat.S_IXUSR)
            subprocess.check_call(
                orig_parfile, stdout=stdout, stderr=stderr, cwd=cwd, shell=True)
            new_parfile = par_match.group(1)+".par"
            if not os.path.exists(new_parfile):
                raise UserException("Parameter file script "+orig_parfile+
                                    " did not produce a parameter file "+ new_parfile+
                                    " when executed")
            env = Environment([env, {'par': new_parfile}])
            self.logger().debug("Using parameter file "+new_parfile+" generated by script")
        else:
            self.logger().debug("Parameter file "+orig_parfile+
                                " will be used verbatim as it is not a script")
        # TODO: is this the right way to process an environment?
        return env

    def logger(self):
        return logging.getLogger(self.logger_name)
