"""
==================
environment module
==================

The environment module contains the Environment class which represents
a set of properties and values.
"""

from collections import Mapping

class Environment(Mapping):
    """
    Environment object.
    """

    _MAX_INTERPOLATION_ITERATIONS = 10

    def __init__(self, dicts):
        # Read all key: value pairs from the list of dictionaries in dicts
        self._rawenv = {}
        for d in dicts:
            self._rawenv.update(d)

    def __getitem__(self, key):
        return self._rawenv[key]

    def __iter__(self):
        return iter(self._rawenv)

    def __len__(self):
        return len(self._rawenv)

    def __repr__(self):
        return 'Environment("%s")' % repr(self._rawenv)

    def __str__(self):
        return str(self._rawenv)

    def __eq__(self, other):
        return vars(self) == vars(other) if other != None else False

    def eval(self, key):
        if not isinstance(self._rawenv[key], str):
            raise TypeError('eval() cannot be used with key "%s" of type "%s"' %
                            (key, type(self._rawenv[key])))

        # Compute an interpolated value for the key
        try:
            # Repeatedly interpolate until the result doesn't change
            return self._repeat_format(self._rawenv[key], self._rawenv)
        except KeyError as exn:
            raise KeyError('missing option "%s" in interpolating value for '
                           'key "%s"' % (exn.args[0], key)) from exn

    def eval_str(self, template):
        return self._repeat_format(template, self._rawenv)

    def _repeat_format(self, template, defs):
        temp1 = template
        for _ in range(self._MAX_INTERPOLATION_ITERATIONS):
            # Make sure escaped { and } are kept intact
            temp2 = temp1.replace('{{', '{{{{').replace('}}', '}}}}').format(**defs)
            if temp2 == temp1:
                return temp2.replace('{{', '{').replace('}}', '}')
            temp1 = temp2
        raise AttributeError('exceeded maximum number of interpolation '
                             'iterations while interpolating value for "%s"' %
                             template)

