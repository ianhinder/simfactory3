"""
============
utils module
============

The utils module contains low-level helper functions.
"""

import contextlib
import datetime
import os
import re
import shutil
import signal
import sys
import time
import simfactory.yaml as yaml
from collections import Mapping
from collections import OrderedDict

# Exceptions of this type can be reported directly as error messages
# to the end-user.  Might want to have this defined in a different
# module.
class UserException(Exception):
    def add_context(self, context):
        self.args = (self.args[0]+context,) + self.args[1:]


# TODO: Introduce function quote_for_prefix (?) that does not quote
# single spaces in its argument. This would be useful for command
# arguments for ssh, env etc., which accept multiple arguments.
def _quote_via_single_quote(arg):
    # Quote by enclosing in single quotes, and replace all single
    # quotes with backslash-quote
    quoted = "'" + re.sub("'", "'\\\\''", arg) + "'"
    # Remove empty leading or trailing quotes
    quoted = re.sub("^''", "", quoted)
    quoted = re.sub("'''$(?!\n)", "'", quoted)
    if quoted == "":
        quoted = "''"
    return quoted
def _quote_via_double_quote(arg):
    # Quote by enclosing in double quotes and escaping certain special
    # characters with a backslash, and by escaping all exclamation
    # marks with a backslash
    quoted = '"' + re.sub('(?=[$`"\\\\])', '\\\\', arg) + '"'
    quoted = quoted.replace('!', '"\\!"')
    # Remove empty leading or trailing quotes
    quoted = re.sub('^""', '', quoted)
    quoted = re.sub('!""$(?!\n)', '!', quoted)
    if quoted == '':
        quoted = '""'
    return quoted
def _quote_via_backslash(arg):
    # Quote by excaping all non-alphanumeric characters with a
    # backslash, and by enclosing all newlines with single quotes
    quoted = re.sub('(?=[^-0-9a-zA-Z_./\n])', '\\\\', arg)
    quoted = quoted.replace("\n", "'\n'")
    if quoted == "":
        quoted = "''"
    return quoted
def quote(arg):
    """
    Quote a shell argument, protecting against shell expansion
    """
    if not isinstance(arg, str):
        raise Exception()
    if '\0' in arg:
        # We can't quote NUL
        raise Exception("NUL")
    # Return the shortest string
    return sorted([_quote_via_single_quote(arg),
                   _quote_via_double_quote(arg),
                   _quote_via_backslash(arg)], key=len)[0]



def convert_to_remote_dir(localmachine, remotemachine, ldir):
    """
    Convert the local path 'ldir' on machine 'localmachine' to the equivalent
    directory on the remote machine 'remotemachine'.
    """
    lsourcebasedir = localmachine.eval('sourcebasedir')
    if os.path.commonprefix([ldir, lsourcebasedir]) != lsourcebasedir:
        raise ValueError('%s is not a subdirectory of %s' % (ldir, lsourcebasedir))

    rel = os.path.relpath(ldir, lsourcebasedir)
    rsourcebasedir = remotemachine.eval('sourcebasedir')
    rdir = os.path.join(rsourcebasedir, rel)
    return rdir

# TODO: this function and the next should probably go into a module
# that implements a pseudo-queuing system
def is_child_process_running(pid):
    try:
        os.kill(pid, 0)
    except OSError:
        return False

    # The process is either running or is a zombie
    # Reap the process if it is a zombie, or do nothing if it is running
    if os.waitpid(pid, os.WNOHANG) == (0, 0):
        # The process exit code is not available, so it is still running
        return True
    else:
        # The process has now finished
        return False

def kill_child_process_group(pid):
    "Kill a child process"

    # If the process is already a zombie (i.e. dead but the return
    # code has not been reaped), reap the return code.  Don't hang in
    # case the process is still running.
    os.waitpid(pid, os.WNOHANG)

    # Now kill the process.  This might fail if the process is already
    # dead, in which case we can exit with success.  Queueing systems
    # use SIGKILL, so we may as well.
    try:
        os.killpg(pid, signal.SIGKILL)
        os.waitpid(pid, 0)
    except OSError:
        return

# From http://code.activestate.com/recipes/576620-changedirectory-context-manager/
@contextlib.contextmanager
def working_directory(path):
    """A context manager which changes the working directory to the given
    path, and then changes it back to its previous value on exit.

    """
    # TODO: ES: Isn't this usually implemented by opening the current
    # directory, and later cd'ing to the opened directory to get back?
    # This doesn't rely on getcwd, which may become confused if
    # symbolic links change in the mean time.
    prev_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)

@contextlib.contextmanager
def stdout_redirect(o_file):
    """A context manager which redirects stdout to the given file, and
    then changes it back to its previous value on exit.

    """
    prev_stdout = sys.stdout
    sys.stdout = o_file
    try:
        yield
    finally:
        sys.stdout = prev_stdout

def parse_walltime(walltime_str):
    # Why is there is no built-in function to do this?
    elements = walltime_str.split(':')
    errormessage = ("Walltime specification {} should be in the form h:m:s, where h, m and s "
                    "are integers".format(walltime_str))

    # We could get more complicated, but this is sufficient for now
    if len(elements) != 3:
        raise Exception(errormessage)

    for element in elements:
        try:
            _ = int(element)
        except ValueError:
            raise Exception(errormessage)

    return datetime.timedelta(hours=int(elements[0]), minutes=int(elements[1]),
                              seconds=int(elements[2]))

def wait_for(fun, timeout=None, delay=0.1):
    """Wait until the callable fun returns True, or the timeout in
    seconds is reached.  If the timeout is reached, return False,
    else return True."""
    start_time = time.time()
    while not fun() and (timeout == None or time.time() < start_time + timeout):
        time.sleep(delay)
    return fun()

# Lazy property decorator: use this for class instance attributes
# which should only be initalized when first read, rather than
# when the instance is constructed.
def lazyprop(fun):
    attr_name = '_lazy_' + fun.__name__
    @property
    def _lazyprop(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fun(self))
        return getattr(self, attr_name)
    @_lazyprop.setter
    def _lazyprop(self, value):
        setattr(self, attr_name, value)
    return _lazyprop

# Remove a directory tree
def rmtree(path):
    shutil.rmtree(path, ignore_errors=True)
    # rmtree does not remove symbolic links
    try:
        os.remove(path)
    except OSError:
        pass
    # NFS may prevent deleting files -- try renaming it
    if os.path.exists(path):
        filename = os.path.basename(path)
        dirname = os.path.dirname(path)
        trash_dir = os.path.join(dirname, 'REMOVE')
        os.makedirs(trash_dir, exist_ok=True)
        timestamp = (datetime.datetime.utcnow() -
                     datetime.datetime(1970, 1, 1)).total_seconds()
        pid = os.getpid()
        suffix = "%f.%d" % (timestamp, pid)
        trash_file = filename + '.' + suffix
        trash_path = os.path.join(trash_dir, trash_file)
        print("RENAMING", path, trash_path)
        os.rename(path, trash_path)
        shutil.rmtree(trash_dir, ignore_errors=True)
    if os.path.exists(path):
        raise 'Could not remove directory tree "%s" % path'

def file_string_replace(filename, replacements):
    """Replace strings in a file.

    Perform simple string substitution in a file using a dictionary of
    replacements.  Update the file in place.
    """
    with open(filename, 'r') as infile:
        orig_str = infile.read()

    new_str = orig_str
    for k in replacements:
        new_str = new_str.replace(k, replacements[k])

    with open(filename, 'w') as outfile:
        outfile.write(new_str)

def string_of_file(filename):
    with open(filename, 'r') as infile:
        return infile.read()

def write_string_to_file(filename, string):
    with open(filename, 'w') as outfile:
        return outfile.write(string)

def file_matches_pattern(filename, pattern):
    return re.search(pattern, string_of_file(filename))

def diff_structs(struct_a, struct_b, level=0):
    if isinstance(struct_a, Mapping) and isinstance(struct_b, Mapping):
        return diff_mappings(struct_a, struct_b, level)
    else:
        raise Exception("Don't know how to diff", type(struct_a), "and", type(struct_b))

def diff_mappings(map_a, map_b, level=0):
    indent = "  " * level

    removed_dict = {key:val for (key, val) in map_a.items() if key not in map_b.keys()}
    added_dict = {key:val for (key, val) in map_b.items() if key not in map_a.keys()}
    modified_keys = [key for key in map_b.keys() if key in map_a.keys() and
                     map_b[key] != map_a[key]]

    diff = ""

    if removed_dict:
        diff = diff+indent+"- "+repr(removed_dict)+"\n"
    if added_dict:
        diff = diff+indent+"+ "+repr(added_dict)+"\n"

    for k in modified_keys:
        diff = diff+indent+"~ "+k+":\n"
        diff = diff+diff_structs(map_a[k], map_b[k], level+1)

    return diff

def merge_dicts(dict_a, dict_b):
    dict_c = dict_a.copy()
    dict_c.update(dict_b)
    return dict_c

def path_split(path):
    # See
    # http://stackoverflow.com/questions/3167154/how-to-split-a-dos-path-into-its-components-in-python
    # for why this is not a universal solution.  It would be better if
    # there was a version of os.path.split which got it right all the
    # time.  Note that in Python 3.4, there is pathlib, which is a
    # general solution to this problem.
    return path.split(os.sep)

def join_logger_names(parent, sub):
    if parent == '':
        return sub
    else:
        return parent+"."+sub

# From http://stackoverflow.com/questions/92438/stripping-non-printable-characters-from-a-string-in-python
def filter_nonprintable(text):
    import string
    # Get the difference of all ASCII characters from the set of printable characters
    nonprintable = set([chr(i) for i in range(128)]).difference(string.printable)
    # Use translate to remove all non-printable characters
    return text.translate({ord(character):None for character in nonprintable})

def file_modification_time(filename):
    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t)


# From https://stackoverflow.com/questions/5121931/in-python-how-can-you-load-yaml-mappings-as-ordereddicts
def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)

def ordered_load_all(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass
    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))
    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load_all(stream, OrderedLoader)
