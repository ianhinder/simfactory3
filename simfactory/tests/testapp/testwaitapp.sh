#!/bin/bash

set -e

trap 'touch "stopped"; exit 1' SIGTERM SIGKILL
trap 'touch "exit"; exit 1' EXIT

echo "Test application running"

if [ $# -ne 1 ]; then
    echo "Wrong number ($#) of arguments; expecting only one, the parameter file" >&2
    exit 1
fi

parfile="$1"
cat "${parfile}"
echo "This is a warning" >&2
touch "waiting"
while [ 1 ]; do
    sleep 1
done
