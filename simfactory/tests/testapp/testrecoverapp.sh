#!/bin/bash
set -e
set -u
#set -x
echo "Checkpoint test application running"

if [ $# -ne 1 ]; then
    echo "Wrong number ($#) of arguments; expecting only one, the parameter file" >&2
    exit 1
fi

cpfiles=( $(ls|grep checkpoint || true) )

ncp=${#cpfiles[@]}

if [ $ncp -gt 0 ]; then
    last=${cpfiles[$ncp-1]}
    it=$(cat $last)
    echo "Recovered from iteration ${it}"
else
    echo "No checkpoint files found"
    it=0
fi

it=$((it+1))

echo "Dumping checkpoint at iteration $it"
echo "$it" >"checkpoint-$it"

if [ $it -gt 2 ]; then
    echo "Terminated due to final time"
else
    echo "Terminated due to walltime"
fi

touch "done"
