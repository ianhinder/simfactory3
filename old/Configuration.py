"""
=====================
Configuration module
=====================

The Configuration module contains the Configuration class which
provides a high-level interface to Cactus configurations.
"""

import CactusConfiguration
import os
from ConfigFile import ConfigFile

class Configuration(ConfigFile):

    """
    A Configuration object represents a Cactus configuration.  It
    has certain persistent properties, such as the name of the
    *thornlist* and *optionlist* files used to build it, and certain
    actions, such as *build* and *clean*.  You can set the properties
    using the usual Python dictionary notation.  The properties (used
    as strings) are:

    optionlist
       The name of the optionlist file to use for this configuration.
       If the original optionlist is modified, the next *build* of the
       configuration will use the new version.  This must be set before
       calling *build*.

    thornlist
       The name of the thornlist file to use for this configuration.
       If the original thornlist is modified, the next *build* of the
       configuration will use the new version.  This must be set before
       calling *build*.

    debug
       Boolean variable indicating whether the configuration should be
       built in Cactus' DEBUG mode.  This enables extra debugging code.
       Defaults to False.

    optimise
       Boolean variable indicating whether the configuration should be
       built in Cactus' OPTIMISE mode.  This enables compiler
       optimisations.  Defaults to True.
    
    If the optionlist or thornlist is modified, subsequent builds will
    use the new versions.

    For example, the Configuration class can be used as follows: ::

        config = Configuration("sim")
        config['optionlist'] = mymachine.cfg
        config['thornlist'] = mythorns.th
        config.build()

    """

    def __init__(self, name, machine=None):
        self.__config = CactusConfiguration.CactusConfiguration(name)
        defaults = {'debug' : False, 'optimise' : True}

        ConfigFile.__init__(self,
            os.path.join("configs", name, "properties.ini"), defaults=defaults)
        self.allowed_keys = ['optionlist', 'thornlist', 'debug', 'optimise']
        self.machine = machine

    def build(self, reconfigure=False):
        """Build the configuration using the current properties.  If
           the optionlist has been modified, the configuration will be
           reconfigured.  If the thornlist has been modified, the new
           thornlist will be used.  If reconfigure is True, a
           reconfiguration will be forced even if the optionlist has
           not changed."""
        __config.build(reconfigure = reconfigure,
                       options     = self.get_used_optionlist(),
                       thornlist   = self['thornlist'],
                       debug       = self['debug'],
                       optimise    = self['optimise'])

    def __getitem__(self, option):
        if option in self.allowed_keys:
            return ConfigFile.__getitem__(self, option)

    def __setitem__(self, option, value):
        if option in self.allowed_keys:
            ConfigFile.__setitem__(self, option, value)

    def get_used_optionlist(self):
        if not 'optionlist' in self and self.machine != None and 'optionlist' in self.machine:
            return self.machine['optionlist']
        else:
            return self['optionlist']
