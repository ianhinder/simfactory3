import unittest
import glob, os
import simfactory.yaml as yaml

import simfactory.paths
from simfactory import mdb
from simfactory import appdb
from simfactory.environment import Environment

class TestDBs(unittest.TestCase):
    """Test the machine and application databases provided with SimFactory"""

    def setUp(self):
        self.maxDiff = None

    def test_parse_mdb(self):
        """Check all mdb entries can be parsed."""
        mdb_files = glob.glob(os.path.join(simfactory.paths.mdbpath(),'*.yaml'))
        success = True
        try:
            for filename in mdb_files:
                with open(filename) as f:
                    yaml.load(f)
        except yaml.YAMLError:
            success = False
        self.assertTrue(success)

    def test_parse_appdb(self):
        """Check all appdb entries can be parsed."""
        appdb_files = glob.glob(os.path.join(simfactory.paths.appdbpath(),'*.yaml'))
        success = True
        try:
            for filename in appdb_files:
                with open(filename) as f:
                    yaml.load(f)
        except yaml.YAMLError:
            success = False
        self.assertTrue(success)

    def test_provided_mdb_appdb(self):
        """Test an environment can be created from all (machine, app) combinations."""
        # TODO: Think about whether this test makes sense
        bad_combinations = []
        for machinename, m in mdb.mdb.items():
            for appname, a in appdb.appdb.items():
                try:
                    # TODO: we assume some keys come from elsewhere. Check that this
                    #       is a valid assumption in each case
                    env = Environment([{'processes': 1,
                                  'threads_per_process': 1,
                                  'processes_per_node': 1,
                                  'reqcores': 1,
                                  'nodes': 1,
                                  'node_list' : "node123,node56",
                                  'executable': '',
                                  'scriptfile': '',
                                  'app-path': '',
                                  'build': '',
                                  'par': '',
                                  'mathscript': '',
                                  'simfactorydir': '',
                                  'shortsimname': '',
                                  'simname' : '',
                                  'stdout': '',
                                  'stderr': '',
                                  'job_id': '',
                                  'walltime': '',
                                  'walltime_minutes': '',
                                  'user': 'dummyuser',
                                  'email': 'dummyuser@example.com',
                                  'mailsubject' : 'dummy subject',
                                  'mailrecipient' : "dummy@example.com",
                                  'mailcontent' : 'dummy content',
                                  'default_allocation': 'none'},
                                 m, a])
                    for k in env:
                        if isinstance(env[k], str):
                            env.eval(k)
                except (KeyError, ValueError) as e:
                    bad_combinations += [(machinename, appname, e.args[0])]
        self.assertListEqual([], bad_combinations)

if __name__ == '__main__':
    unittest.main(verbosity=2)
