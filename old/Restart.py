
def create(name, executable, runscript):
    os.mkdir(name)
    copyfilewithcaching(executable, restartdir/SIMFACTORY/exe/basename(executable))
    copyfile(runscript, restartdir)

class Restart:
    def __init__(self, name):
        self.name = name

    def submit(self, submitscript, procs, walltime):
        our_submitscript = restartdir/SIMFACTORY/submitscript
        copy_with_replacements(submitscript, our_submitscript, get_replacements() + {'procs' : procs, 'walltime' : walltime})
        append_to_file(our_submitscript, runscript)
        jobid = system(["qsub", "<"+our_submitscript])
        store_property(jobid)

    def run(self, procs):
        copy_with_replacements(runscript, our_runscript, get_replacements() + {'procs' : procs})
        system(runscript)

    def stop(self):
        system(["qdel", get_property(jobid)])
