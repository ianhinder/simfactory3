import unittest

from simfactory.environment import Environment

class TestEnvironment(unittest.TestCase):
    """Test the Environment class"""

    def setUp(self):
        self.maxDiff = None

    def test_interpolation(self):
        """Test an environment does interpolation safely"""
        e = Environment([{'test': '{test2}', 'test2': 'test3'}])
        self.assertEqual(e.eval('test'), 'test3')

        e = Environment([{'test': '{{test2}}', 'test2': 'test3'}])
        self.assertEqual(e.eval('test'), '{test2}')

        e = Environment([{'test': '{test2}', 'test2': '{{test3}}'}])
        self.assertEqual(e.eval('test'), '{test3}')

        with(self.assertRaises(KeyError)):
            e = Environment([{'test': '{test2}', 'test2': '{test3}'}])
            e.eval('test')

        e = Environment([{'test': '{{test2}}', 'test2': '{{{test3}}}'}])
        self.assertEqual(e.eval('test'), '{test2}')

        with(self.assertRaises(KeyError)):
            e = Environment([{'test': '{test2}', 'test2': '{{{test3}}}'}])
            e.eval('test')

        with(self.assertRaises(ValueError)):
            e = Environment([{'test': '{test2}', 'test2': '{{test3}'}])
            e.eval('test')

        e = Environment([{'test': '{test2}', 'test2': '{{{test3}}}', 'test3': 'test4'}])
        self.assertEqual(e.eval('test'), '{test4}')

        with(self.assertRaises(TypeError)):
            e = Environment([{'test': 1}])
            e.eval('test')
        
        with(self.assertRaises(AttributeError)):
            e = Environment([{'test': '{{{{{{{{{{{test}}}}}}}}}}}'}])
            e.eval('test')

    def test_eval_str(self):
        """Test eval_str works"""
        e = Environment([{'test': 1}])
        self.assertEqual('test0 test1 test2', e.eval_str('test0 test{test} test2'))

    def test_len(self):
        """Environments should most behave like a dict"""
        e1 = Environment([{'test': 1, 'test2': 2}, {'test2': '{test1}'}])
        e2 = Environment([{'test': 1, 'test2': '{test1}'}])
        
        self.assertEqual(len(e1), 2)

        # dicts are unsorted and the order of str and repr is nondeterministic
        self.assertTrue(repr(e1) == 'Environment("{\'test\': 1, \'test2\': \'{test1}\'}")' or
                        repr(e1) == 'Environment("{\'test2\': \'{test1}\', \'test\': 1}")')
        self.assertTrue(str(e1) == "{'test': 1, 'test2': '{test1}'}" or
                        str(e1) == "{'test2': '{test1}', 'test': 1}")
        self.assertEqual(e1, e2)

if __name__ == '__main__':
    unittest.main(verbosity=2)
