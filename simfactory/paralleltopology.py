################################################################################

"""
=======================
paralleltopology module
=======================

The paralellisation module contains the ParallelTopology class which is
a description of how to parallelise a simulation.

"""

from simfactory.utils import UserException
from math import ceil

class MissingCoreProcessError(UserException):
    pass

class CoreProcessError(UserException):
    pass

################################################################################

class ParallelTopology:
    """A ParallelTopology object is a description of how to parallelise
    a simulation."""

    def __init__(self, machine, request):
        """Construct a new ParallelTopology object with the settings in
        the dict 'request', if possible, for the given machine.
        request can contain any of the canonical four paralleltopology
        options ('processes-per-node', 'processes',
        'cores-per-process' and 'threads-per-core') and/or 'cores' or 'nodes'.
        Exactly one of 'processes', 'cores' or 'nodes' may be specified; the
        others are optional and determined automatically from the
        machine."""

        self.machine = machine
        self.request = request

        allowed = ['processes-per-node', 'cores-per-process',
                   'threads-per-core', 'processes', 'cores', 'nodes', 'node-list']

        for key, value in request.items():
            if not key in allowed:
                raise Exception(
                    "ParallelTopology request cannot contain "+str(key))
            if not isinstance(value, int) and key != 'node-list':
                raise Exception(
                    "ParallelTopology option "+str(key)+" must be an integer")
            if key != 'node-list' and value < 1:
                raise Exception(
                    "ParallelTopology option "+str(key)+
                    " must be greater than zero")

        ########################################################################
        # Canonical quantities
        ########################################################################

        if 'processes-per-node' in request:
            self.processes_per_node = request['processes-per-node']
        else:
            # TODO: we probably want to have
            # default_processes_per_node in the MDB.  The number of
            # sockets would be a good default for this default if it
            # is not set, but we might want to override this for a
            # particular machine.
            self.processes_per_node = machine['num_sockets']

        if 'cores-per-process' in request:
            self.cores_per_process = request['cores-per-process']
        else:
            # TODO: probably we want to change the MDB keys available
            # TODO: check that one divides the other
            self.cores_per_process = (int(machine['default_num_threads'] /
                                          machine['default_num_smt']))

        if 'threads-per-core' in request:
            self.threads_per_core = request['threads-per-core']
        else:
            self.threads_per_core = machine['default_num_smt']

        # TODO: add check for nodes here as well
        if 'processes' in request and 'cores' in request:
            raise CoreProcessError(
                "Number of cores and number of processes are mutually exclusive")

        # We only allow one of these to be specified.  This is not the
        # most general possibility, but it avoids having to solve the
        # system of equations.
        if 'processes' in request:
            self.processes = request['processes']
        elif 'cores' in request:
            self.processes = ceil(request['cores']/self.cores_per_process)
        elif 'nodes' in request:
            self.processes = request['nodes']*self.processes_per_node
        else:
            # TODO: ICH: does it make sense to default to the minimum
            # possible number of cores instead?
            raise MissingCoreProcessError("Missing core or process count")

        ########################################################################
        # Derived quantities
        ########################################################################

        self.cores = self.processes * self.cores_per_process
        self.threads_per_process = (self.cores_per_process *
                                    self.threads_per_core)
        self.nodes = ceil(self.processes / self.processes_per_node)
        self.node_list = request['node-list'] if 'node-list' in request else ""

    def env(self):
        """Return the string-replacement environment defined by this
        ParallelTopology."""
        return {"reqcores": self.cores, 'processes': self.processes,
                'threads_per_process': self.threads_per_process,
                'processes_per_node': self.processes_per_node,
                'nodes' : self.nodes, 'node_list' : self.node_list if hasattr(self,'node_list') else None}

    def __eq__(self, other):
        return vars(self) == vars(other) if other != None else False
