"""
============
utils module
============

The utils module contains high-level helper functions.
"""

import os
import re

from simfactory.mdb import mdb
from simfactory.application import Application
from simfactory.build import Build
import simfactory.application
from simfactory.appdb import appdb, probe_application
import simfactory.simulation

def local_machine(machine):
    """"
    Return the local machine (i.e. the machine where SimFactory is executing)
    """

    if machine:
        return mdb[machine]
    else:
        return mdb.probe_local_machine()

def current_application(args):
    """Return an Application object representing the current application as
    determined through command-line arguments or via probing the
    current or simfactory directories."""

    # TODO: we probably want a command-line option to specify the
    # application path, in case we want to run from a process whose
    # CWD is not under the application directory, or whose application
    # path cannot be determined via a fingerprint file.

    if args.application_name:
        app = appdb[args.application_name]
    else:
        candidates = probe_application()
        if len(candidates) != 1:
            print("Cannot determine current application; please specify one using --app <appname>")
            exit(1)
        app = candidates[0]

    return app

def current_build(args):
    """Return a Build object representing the build to use as
    determined through command-line arguments or via probing the
    current or simfactory directories."""

    app = current_application(args)
    app_path = app.probe_path()

    buildname = args.build if args.build else app['default-build']
    exe = args.executable if hasattr(args,'executable') else None


    return Build(name=buildname,app=app,app_path=app_path,exe=exe)

def simulation_exists(machinename, simname):
    return simfactory.simulation.simulation_exists(
        mdb.get_local_machine(), simname)

def load_simulation(machine, simulation):
    return simfactory.simulation.load(os.path.join(local_machine(machine).eval('basedir'),
                                                   simulation))
