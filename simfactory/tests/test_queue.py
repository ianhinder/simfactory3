import datetime
import shutil
import tempfile
import unittest
import simfactory.utils

class TestQueue(unittest.TestCase):

    def setUp(self):
        # create a temporary working directory
        self.tempdir = tempfile.mkdtemp()
        # print("tempdir = "+self.tempdir)

    # def test_submit(self):
    #     jobdir = self.tempdir+"/submittest"
    #     os.mkdir(jobdir)
    #     job_id = queue.submit(["bash", "-c", "while true; do sleep 1; done"],
    #                           rundir=jobdir)
    #     self.assertTrue(simfactory.utils.is_child_process_running(int(job_id)))
    #     simfactory.utils.kill_child_process_group(int(job_id))

    # def test_submit_output(self):
    #     jobdir = self.tempdir+"/submitoutputtest"

    #     os.mkdir(jobdir)

    #     job_id = queue.submit(["bash", "-c", 'echo "Hello World"; echo "Goodbye World" >&2'],
    #                           rundir=jobdir)
        
    #     try:
    #         os.waitpid(int(job_id),0) # Works for the current implementation
    #     # TODO: be more specific
    #     except OSError:
    #         pass

    #     with open (jobdir+"/job.out", "r") as f:
    #         data=f.readlines()
    #     self.assertEqual(data,['Hello World\n'])

    #     with open (jobdir+"/job.err", "r") as f:
    #         data=f.readlines()
    #     self.assertEqual(data,['Goodbye World\n'])

    # def test_stop(self):
    #     jobdir = self.tempdir+"/stoptest"
    #     os.mkdir(jobdir)
    #     job_id = queue.submit(["bash", "-c", "while true; do sleep 1; done"],
    #                           rundir=jobdir)
    #     self.assertTrue(simfactory.utils.is_child_process_running(int(job_id)))
    #     queue.stop(job_id)
    #     self.assertFalse(simfactory.utils.is_child_process_running(int(job_id)))

    # def test_status(self):
    #     jobdir = self.tempdir+"/statustest"
    #     os.mkdir(jobdir)
    #     job_id = queue.submit(["bash", "-c", "while true; do sleep 1; done"],
    #                     rundir=jobdir)
    #     self.assertTrue(simfactory.utils.is_child_process_running(int(job_id)))

    #     self.assertEqual(queue.status(job_id), "R")
    #     queue.stop(job_id)
    #     self.assertEqual(queue.status(job_id), "U")

    def test_timedelta_to_str(self):
        self.assertEqual(simfactory.queue._timedelta_to_str(datetime.timedelta(hours=1)), "1:0:0")
        self.assertEqual(simfactory.queue._timedelta_to_str(datetime.timedelta(hours=1,minutes=2,seconds=3)), "1:2:3")
        self.assertEqual(simfactory.queue._timedelta_to_str(datetime.timedelta(hours=0,minutes=0,seconds=0)), "0:0:0")
        self.assertEqual(simfactory.queue._timedelta_to_str(datetime.timedelta(hours=0,minutes=0,seconds=70)), "0:1:10")
        self.assertEqual(simfactory.queue._timedelta_to_str(datetime.timedelta(hours=30,minutes=12,seconds=4)), "30:12:4")
        self.assertEqual(simfactory.queue._timedelta_to_str(datetime.timedelta(days=2,hours=3,minutes=12,seconds=4)), "51:12:4")

    def test_timedelta_to_minutes(self):
        self.assertEqual(simfactory.queue._timedelta_to_minutes(datetime.timedelta(hours=1)), 60)
        self.assertEqual(simfactory.queue._timedelta_to_minutes(datetime.timedelta(hours=1,minutes=2,seconds=3)), 62)

    def tearDown(self):
        shutil.rmtree(self.tempdir)
        pass # pylint: disable=no-member

if __name__ == '__main__':
    unittest.main(verbosity=2)
