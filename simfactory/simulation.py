
##########################################################################

"""
=====================
simulation module
=====================

The simulation module contains the Simulation class which
provides a high-level interface to simulations.
"""

##########################################################################

import glob
import os
import pickle
import os.path
import sys
import shutil
import subprocess
import datetime

from simfactory.paths import basepath

from simfactory.segment import Segment
import simfactory.build
from simfactory.environment import Environment
import simfactory.appdb
import simfactory.mdb
from simfactory.utils import path_split
from simfactory.utils import UserException
from simfactory.settings import settings
import simfactory.utils
import simfactory.yaml as yaml
import simfactory.paths

##########################################################################

# This is used by the test system


def segment_name(index):
    """Return the name for a segment with the given index."""
    return "output-{0:04d}".format(index)


def load(simdir):
    """Load a simulation by path into a new Simulation object which is
    returned"""
    # TODO: address concurrent access to a simulation by multiple
    # simfactory processes
    picklename = os.path.join(simdir, "SIMFACTORY", "properties.py")

    try:
        picklefile = open(picklename, "rb")
    except IOError:
        simname = os.path.basename(simdir)
        if not os.path.exists(simdir):
            raise SimulationDoesntExistError(
                "Simulation "+simname+" not found")
        else:
            raise UserException(
                simname+" is not a valid SimFactory 3 simulation ("+
                simdir+")")
    else:
        with picklefile:
            sim = pickle.load(picklefile)
            # TODO: decide how to best handle this.  Refreshing is a
            # short-term fix.  It runs the risk of overwriting customised
            # data in the simulation.  Eventually, we probably want to
            # store the original application entry as well as any changes
            # made.  Refreshing will then refresh just the original
            # "backing" entry, so that changes in the simulation are
            # preserved.
            # sim.refresh()
            return sim


def get_simulations(machine):
    env = Environment([machine])
    pattern = os.path.join(
        env.eval('basedir'), '*', 'SIMFACTORY', 'properties.py')
    return [load(os.path.dirname(os.path.dirname(path))) for path in glob.glob(pattern)]

def get_simulation_names(machine, pattern='*'):
    env = Environment([machine])
    pattern = os.path.join(
        env.eval('basedir'), pattern, 'SIMFACTORY', 'properties.py')
    return [path_split(path)[-3] for path in glob.glob(pattern)]

def simulation_exists(machine, simname):
    return os.path.exists(
        os.path.join(
            machine.eval('basedir'),
            simname))

def clone(orig_path, new_name):
    orig_sim = load(orig_path)

    # TODO: the build executable path is probably pointing to the
    # original simulation.  There is a comment in build.py about
    # making the executable path relative to make builds relocatable.
    # That would be one solution.

    # TODO: find a better way to pass the simulation properties
    # through.  When we implement writing and reading simulation
    # definitions to and from a YAML file, this would be one
    # convenient way to achieve this.

    new_sim = Simulation(
        new_name, orig_sim.build, orig_sim.par,
        orig_sim.machine,
        orig_sim.paralleltopology,
        walltime=orig_sim.walltime,
        continuous_resubmission=orig_sim.continuous_resubmission,
        definitions=orig_sim.definitions,
        postsimulationscript=orig_sim.postsimulationscript)
    new_sim.activate()


##########################################################################


class Simulation:
    """
    A Simulation object represents a simulation.  It can be started
    and stopped in a queuing system, or run interactively.  The
    running/not-running state can be queried.
    """

    # Create a new simulation
    def __init__(self, name, build, par, machine, paralleltopology,
                 basedir=None, walltime=None,
                 continuous_resubmission=True,
                 definitions=[],postsimulationscript=None):
        """Create a new Simulation object and store its representation
        on disk in a new directory."""

        assert isinstance(name, str)
        assert isinstance(build, simfactory.build.Build)
        assert isinstance(machine, simfactory.machine.Machine)

        # Allow the basedir to be overridden. Used by the tests.
        self.dir = os.path.join(basedir or machine.eval('basedir'), name)

        try:
            os.mkdir(self.dir)
        except FileExistsError:
            raise UserException("Simulation "+name+" already exists")

        self.name = name
        self.paused = False
        # TODO: These should be copied into the simulation directory
        self.machine = machine
        self.metadatadir = os.path.join(self.dir, 'SIMFACTORY')
        os.mkdir(self.metadatadir)
        self.pardir = os.path.join(self.metadatadir, 'par')
        os.mkdir(self.pardir)
        if not par:
            # This should be mediated by the application object, since
            # some applications may not use parameter files
            raise UserException("Missing parameter file for simulation "+name)
        try:
            shutil.copy(par, self.pardir)
        except FileNotFoundError:
            raise UserException("Parameter file "+par+" does not exist for simulation "+name)

        self.par = os.path.abspath(
            os.path.join(self.pardir, os.path.basename(par)))
        self.builddir = os.path.join(self.metadatadir, 'build')
        os.mkdir(self.builddir)
        self.build = simfactory.build.clone(build, self.builddir)
        self.paralleltopology = paralleltopology
        self.outfile = self.name+".out"
        self.errfile = self.name+".err"
        self.segments = []
        self.segment_counter = 0
        self.walltime = walltime
        self.active = False
        self.continuous_resubmission = continuous_resubmission
        self.definitions = definitions
        self.postsimulationscript = postsimulationscript
        self.__save()

################################################################################
# Public methods
##########################################################################

    # Elementary operations

    def activate(self):
        """Mark the simulation as active"""

        if self.active:
#            raise SimulationActiveError("Simulation "+self.name+" is already active")
            print("WARNING: Simulation "+self.name+" is already active")
        self.active = True
        self.__save()

    def submit(self, continuation=False):
        """Submit an active simulation to the queueing system.  If
        continuation is True, the submission will succeed even if the
        simulation is running or queued. The assumption is that the
        calling process is running from the job, and the job will end
        soon without further modification of the segment."""

        # TODO: the 'continuation' exception is temporary and a bit
        # dangerous.  The submission which runs at the end of a job to
        # start the next job should instead specify a dependency on
        # the current job so that the new job does not start too early
        # and potentially attempt to access the simulation while it is
        # being accessed by this job.

        if not self.active:
            raise SimulationActiveError("Simulation "+self.name+" is not active")

        if self.status() != 'P' and not continuation:
            raise SimulationQueueError(
                "Cannot submit simulation "+self.name+" which is queued or running")

        self.paused = False

        segment = self._new_segment()
        segment.submit()
        self.__save()


    def modify(self, modifications={}):
        """Modify the properties of a simulation.  The next segment to be
        submitted will inherit these new properties."""

        if 'paralleltopology' in modifications:
            self.paralleltopology = modifications['paralleltopology']

        if 'walltime' in modifications:
            self.walltime = modifications['walltime']

        if 'definitions' in modifications:
            self.definitions = self.definitions + modifications['definitions']
            # These will be converted to a dict in sequence, so later
            # entries override earlier entries
            
        if 'postsimulationscript' in modifications:
            self.postsimulationscript = modifications['postsimulationscript']

        if 'parfile' in modifications:
            par = modifications['parfile']
            try:
# TODO: only remove if the new one was successfully copied in. Maybe
#                rename to a backup file, if it is not called the same
#                as the original.
#                os.remove(self.par)
                shutil.copy(par, self.pardir)
            except FileNotFoundError:
                raise UserException("Parameter file "+par+" does not exist for simulation "+name)

            self.par = os.path.abspath(
                os.path.join(self.pardir, os.path.basename(par)))

        self.__save()

    def run(self, stdout=None, stderr=None):
        """Run an active simulation interactively"""

        if not self.active:
            raise SimulationActiveError("Cannot run inactive simulation "+self.name)

        # Note: this method is not called by the queuing system,
        # as the queuing system calls segment.run instead.  This
        # method would only be called by a user wanting to run the
        # simulation interactively.

        if self.status() != 'P':
            raise SimulationQueueError(
                "Cannot run simulation "+self.name+" which is already queued or running")

        segment = self._new_segment()
        return segment.run(stdout=stdout, stderr=stderr)

    def abort(self):
        """Stop execution of a simulation.  The simulation remains
        'active', but will not run any further until it is either
        'submit'ted or 'run' again."""
        # TODO: maybe aborting should deactivate the simulation
        if not self.active:
            print("Skipping abort of inactive simulation "+self.name)
            return
#            raise SimulationActiveError("Cannot abort inactive simulation "+self.name)

        # Hopefully only the last segment will be running, but let's
        # be safe
        # TODO: give an error if the simulation is already stopped
        for segment in self.segments:
            if segment.status() in ['Q', 'R', 'H']:
                segment.stop()

        self.__save()

    def pause(self):
        """Pause execution of a simulation.  The simulation remains
        'active', but will not run any further until it is 'continue'd
        """

        if not self.active:
            raise SimulationActiveError("Cannot pause inactive simulation "+self.name)

        # TODO: give an error if the simulation is already paused or aborted
        self.segments[-1].pause()
        self.paused = True

        self.__save()


    def status(self):
        """Get the status of a simulation.  Valid status values are
        Paused, Queued and Running, and are indicated by their initial
        letter."""

        # A simulation is either active or inactive; this state is
        # available in the self.active attribute.  For active
        # simulations, the status method determines whether there is a
        # segment which is running, queued, or not.

        if not self.active:
            raise SimulationActiveError(
                "Cannot query the status of inactive simulation "+self.name)

        # We assume for the moment that only the last segment can be
        # queued or running.

        if len(self.segments) == 0:
            return 'P'

        segment = self.segments[-1]
        status = segment.status()
        if status == 'R':
            return status
        elif status == 'Q':
            return status
        elif status == 'H':
            return status
        else:
            return 'P'

    def deactivate(self, allow_running=False):
        """Mark the simulation as inactive."""
        if not self.active:
            print("Skipping deactivation of inactive simulation "+self.name)
            return
        #            raise SimulationActiveError("Simulation "+self.name+
        #" cannot be deactivated because it is not active")

        if not allow_running and self.status() != 'P':
            raise SimulationQueueError(
                "Simulation "+self.name+" must be paused before being deactivated")

        self.active = False
        self.__save()

    def refresh(self):
        """Refresh databases"""
        app_name = self.build.app['name'] if 'name' in self.build.app else 'cactus'
        # old_app = self.build.app
        # new_app = simfactory.appdb.appdb[app_name]
        # diff = simfactory.utils.diff_structs(old_app,new_app)
        # if len(diff) != 0:
        #     print("WARNING: refreshing application "+app_name+
        #           " from SimFactory application database in "+self.name)
        #     print("Changes:")
        #     print(diff)
        self.build.app = simfactory.appdb.appdb[app_name]

#        print("self.machine = ", self.machine._defs)
        machine_name = self.machine['name'] if 'name' in self.machine else 'datura'
        self.machine = simfactory.mdb.mdb[machine_name]

        for seg in self.segments:
            seg.refresh()
        self.__save()

################################################################################
# Private methods
##########################################################################
    def __save(self):
        """Save the current state of the simulation to disk"""
        with open(os.path.join(self.metadatadir, "properties.py"), "wb") as picklefile:
            pickle.dump(self, picklefile)

        with open(os.path.join(self.metadatadir, "properties.yaml"), "w") as yamlfile:
            vs = { key:val for (key,val) in self.__dict__.items()
                   if type(val) in [str,int,float,bool,type(None),datetime.timedelta]}
            yaml.dump(vs, yamlfile,default_flow_style=False)
            yaml.dump({'definitions' : {key:val for [key,val] in self.definitions}}, yamlfile,default_flow_style=False)
            
    def _new_segment(self):
        """Create and return a new segment in the simulation"""
        segment_index = self.segment_counter
        self.segment_counter += 1

        # TODO: this is duplicated from segment.py
        pythoncmd = (
            self.machine['pythoncmd'] if 'pythoncmd' in self.machine else
            os.path.abspath(os.path.join(basepath(), "runpython")))

        postrunscript = """
PYTHONPATH={sfbase}:$PYTHONPATH {python} -c '
from simfactory import simulation

s = simulation.load("{simdir}")
s.post_run()
'
""".format(simdir=self.dir, python=pythoncmd, sfbase=basepath())

        segment = Segment(segment_name(segment_index),
                          self.dir, self.build, self.par,
                          self.machine, self.name, segment_index,
                          paralleltopology=self.paralleltopology,
                          previous_segments=self.segments,
                          walltime=self.walltime,
                          postrunscript=postrunscript,
                          definitions=self.definitions)

        self.segments.append(segment)
        self.__save()
        return segment

    def termination_reason(self):
        """Determine why this segment terminated"""

        if hasattr(self,'paused') and self.paused:
            return "Paused"

        if len(self.segments) > 0:
            return self.segments[-1].termination_reason()
        else:
            return "None"

    def post_run(self):
        """Do anything necessary after the simulation ends"""
        # print("Termination reason: ", self.segments[-1].termination_reason())
        # print("Termination action: ", self.segments[-1].termination_action())
        action = self.segments[-1].termination_action()
        if action == "Continue":
            if self.continuous_resubmission and not self.paused:
                if self.segments[-1].made_progress() == False:
                    print("Simulation segment "+self.name+
                          " made no progress - not continuing")
                else:
                    self.submit(continuation=True)
        elif action == "Deactivate":
            self.deactivate(allow_running=True)
        elif action == "Email":
            if 'email' not in settings:
                print("Cannot send email because no email address has been supplied in simfactory.yaml. Please set 'email' in the Settings: section.")
            elif 'mailcmd' not in self.machine:
                print("Cannot send email because no mailcmd is defined for this machine")
            else:
                sys.stdout.flush()
                sys.stderr.flush()
                errmsg = simfactory.utils.string_of_file(self.segments[-1].get_error_file())
                # TODO: read just the end of the file, as it could be large
                output = simfactory.utils.string_of_file(self.segments[-1].get_output_file())
                output_lines = output.splitlines()
                if len(output_lines) > 20:
                    output_lines = output_lines[-20:-1]
                    output_tail = "\n".join(output_lines)

                # TODO: escape the content correctly for email
                mailmsg = simfactory.utils.filter_nonprintable("""Output:

...
""" + output_tail + """

Error:

""" + errmsg)

                env = Environment([{'mailsubject' : simfactory.utils.quote("Simulation "+self.name+
                                                                           " finished with termination reason "+
                                                                           self.termination_reason()),
                                    'mailrecipient' : settings['email'],
                                    'mailcontent' : simfactory.utils.quote(mailmsg)}])
                mailcmd = env.eval_str(self.machine['mailcmd'])
                subprocess.check_call(mailcmd, shell=True)
        if self.termination_reason() in ["FinalTime","SimulationComplete"]:
            if self.postsimulationscript:
                print("Running post simulation script "+self.postsimulationscript)
                menv = Environment([self.machine])

                env = {"SIMFACTORY_TERMINATION_REASON" : self.termination_reason(),
                       "SIMFACTORY_SIMULATION_NAME" : self.name,
                       "SIMFACTORY_BASE_PATH" : simfactory.paths.basepath(),
                       "SIMFACTORY_MACHINE" : self.machine['name'],
                       "SIMFACTORY_SIMULATIONS_PATH" : menv.eval('basedir'),
                       "SIMFACTORY_EMAIL" : (settings['email'] if 'email' in settings else "")}

                env = simfactory.utils.merge_dicts(
                    env,
                    {"SIMFACTORY_DEFINITION_"+key : val for [key,val] in self.definitions})
                
                env_str = " ".join([ k+"="+env[k] for k in env.keys() ])
                # TODO: use subprocess to avoid unsafe string
                # manipulations
                env_str = menv.eval('envsetup') + "\n" + env_str
                sys.stdout.flush()
                sys.stderr.flush()
                retcode = os.system(env_str+" "+self.postsimulationscript)
                if retcode != 0:
                    print("Post simulation script "+self.postsimulationscript+" failed with exit code "+str(retcode))
                    sys.stdout.flush()
                    sys.stderr.flush()

    def progress(self):
        """Return the progress of the simulation"""
        prog_str = ''
        i = len(self.segments)-1
        while i >= 0:
            prog_str = self.segments[i].progress()
            if prog_str:
                return prog_str
            i = i-1
        return ''

    def last_output_time(self):
        if len(self.segments) > 0:
            return self.segments[-1].last_output_time()
        else:
            return None

        
################################################################################
# Exceptions
################################################################################

class SimulationActiveError(UserException):
    pass

class SimulationExistsError(UserException):
    pass

class SimulationDoesntExistError(UserException):
    pass

class SimulationQueueError(UserException):
    pass
