"""
============
appdb module
============

The appdb module contains the appdb object which represents a database of
applications. The appdb object behaves like an OptionDB, where the
members are instances of the Application class and where only
the "Application" section of the udb is scanned.
"""

import glob
import os

from simfactory.optiondb import OptionDB
from simfactory.application import Application
import simfactory.paths

__all__ = ['appdb', 'probe_application']

class ApplicationDB(OptionDB):
    """
    Application database object.
    """

    OPTION_CLASS = Application
    UDB_SECTION = "Application"

_APPDB_FILES = glob.glob(os.path.join(simfactory.paths.appdbpath(), '*.yaml'))
_UDB_FILES = [os.path.expanduser('~/.simfactory.yaml'),
              os.path.join(simfactory.paths.basepath(), 'simfactory.yaml')]

appdb = ApplicationDB(_APPDB_FILES, _UDB_FILES)

def probe_application():
    """
    Attempt to determine the current application from the CWD and
    return a list of names which might be the current application.
    """

    # TODO: Add a SIMFACTORY_APPLICATION environment variable
    candidates = []
    for app in appdb.values():
        path = app.probe_path()
        if path:
            candidates.append(app)

    return candidates
